% :-consult('minefield.pl').

loadData(Filename):-
  consult(Filename).

%///////////////////////////////////////
% prints minefield for debugging purposes
printMinefield:-
	printRows(0),
  nl.

printRows(Row):- 
	not(minefield((0, Row),_)).
printRows(Row):-
	nl,
	minefield((0,Row), _),
	printColumns(Row, 0),
	NextRow is Row + 1,
	printRows(NextRow),
  !.

printColumns(RowNumber, ColumnNumber):-
	not(minefield((ColumnNumber, RowNumber), _)).
printColumns(RowNumber, ColumnNumber):-
	minefield((ColumnNumber, RowNumber), Value),
	write(Value),
	write(' '),
	NextColumn is ColumnNumber + 1,
	printColumns(RowNumber, NextColumn).

%///////////////////////////////////////

debugAssert(Condition):-
	(   
	    call(Condition),
	    !
	;   
	    write('Assert failed: '),
	    write(Condition),
	    nl
	).

numericalField(Coord):-minefield(Coord, X), integer(X).
minedField(Coord):-minefield(Coord, *).
coveredField(Coord):-minefield(Coord, ?).
zeroField(Coord):-minefield(Coord, 0).
clearField(Coord):-minefield(Coord, c).

% decrease value of all given numerical field by 1
decreaseFieldsValues([]).
decreaseFieldsValues([H|T]):-
	debugAssert(numericalField(H)),
	
	minefield(H, Value),
	debugAssert(Value \= 0),
	
	Decreased is Value - 1,
	retract(minefield(H, Value)),
	asserta(minefield(H, Decreased)),
	decreaseFieldsValues(T).

% increase value of all given numerical field by 1
increaseFieldsValues([]).
increaseFieldsValues([H|T]):-
	debugAssert(numericalField(H)),

	minefield(H, Value),
	debugAssert(Value \= 9),

	Increased is Value + 1,
	retract(minefield(H, Value)),
	asserta(minefield(H, Increased)),
	increaseFieldsValues(T).

% puts a mine at given coordinates and decrease value of all around numerical
% fields by 1
putMine(Coord):- 
	debugAssert(coveredField(Coord)),
	
	distanceOne(Dist),
	getAroundFields(Coord, Dist, numericalField, AroundFields),
	decreaseFieldsValues(AroundFields),
	retract(minefield(Coord, ?)),
	asserta(minefield(Coord, *)).

% picks up a mine at given coordinates and increase value of all around numerical
% fields by 1
pickUpMine(Coord):- 
	debugAssert(minedField(Coord)),
	
	distanceOne(Dist),
	getAroundFields(Coord, Dist, numericalField, AroundFields),
	increaseFieldsValues(AroundFields),
	retract(minefield(Coord, *)),
	asserta(minefield(Coord, ?)).

% puts field is clear (= 100% not mined) at given coordinates
putClear(Coord):- 
	debugAssert(coveredField(Coord)),
	retract(minefield(Coord, ?)),
	asserta(minefield(Coord, c)).

% picks up cleared flag from given coordinate

pickUpClear(Coord):-
	debugAssert(clearField(Coord)),
	retract(minefield(Coord, c)),
	asserta(minefield(Coord, ?)).

% puts new numerical value to given field; it reflects currently
% layed mines and decrease the value according to it
revealFieldInternal(Coord, Value, OriginalValue):-
	distanceOne(Dist),
	getAroundFields(Coord, Dist, minedField, MinedFields),
	length(MinedFields, Mines),
	FieldValue is Value - Mines,
	retract(minefield(Coord, OriginalValue)),
	asserta(minefield(Coord, FieldValue)).

revealField(Coord, Value):-
	coveredField(Coord),
	revealFieldInternal(Coord, Value, ?),
	!.

revealField(Coord, Value):-
	clearField(Coord),
	revealFieldInternal(Coord, Value, c),
	!.

% checks if a mine can be at given coordinate and if no, puts the clear flag
% to the coordinate; also checks if a given field can be clear and if no, puts
% the mine to the field
solveOneField(Coord):-
	(   
	    checkForMine(Coord),
	    (   
	        checkForClear(Coord)
	    ;   
		putMine(Coord)
	    )
	;
	    putClear(Coord)
	),
	!.

% ////////////////////////////////////
% checks if a given field can be clear
checkForClear(Coord):- 
	debugAssert(coveredField(Coord)),
	not(canPutMine(Coord)).

checkForClear(Coord):-
	canPutMine(Coord),
	distanceOne(Dist),
	getAroundFields(Coord, Dist, numericalField, AroundFields),
	(   
	    AroundFields = []
	;
            putClear(Coord),
	
	    [H|_] = AroundFields,
	    getAffectedFields(H, Affected),
	    duplicateAffectedFields(Affected, DupAffected),

	    (	
	        checkFieldsForMines(DupAffected, [], Back),
		var(Back),
		CanBeClear = true
	    ;	
	        CanBeClear = false
	    ),
	    
	    pickUpClear(Coord)
	),
	!,
	CanBeClear = true.

% ////////////////////////////////////

% checks if a mine can be at given coordinate

checkForMine(Coord):-
	debugAssert(coveredField(Coord)),

	getAffectedFields(Coord, AffectedFields),
	duplicateAffectedFields(AffectedFields, DupAffectedFields),
	(   
	    DupAffectedFields = [],
	    canPutMine(Coord)
	;
	    canPutMine(Coord),
	    distanceOne(Dist),
	    putMine(Coord),
		
	    getAroundFields(Coord, Dist, numericalField, AroundFields),
	    removeAffectedFields(AroundFields, DupAffectedFields, Aff),

	    checkFieldsForMines(Aff, [], Back),
	    pickUpMine(Coord),

	    var(Back)
	),
	!.

% creates a list with n-items with Padding value
padding(0, _, []).
padding(N, Padding, [Padding|X]):-
	NewN is N - 1,
	padding(NewN, Padding, X).

% duplicates numerical field count according to the value of field
% fields with value 0 are not in Result, with value 1 are once in
% Result, ...
duplicateAffectedFields([],[]).
duplicateAffectedFields([H|T], Result):-
	minefield(H, Value),
	padding(Value, H, Padding),
	duplicateAffectedFields(T, Duplicated),
	append(Padding, Duplicated, Result),
	!.

% check, if all given fields do not have zero value
nonZeroFields([]).
nonZeroFields([H|T]):-
	not(zeroField(H)),
	nonZeroFields(T).

% check, if the mine can be layed on the specified coordinates;
% the check is done localy in distance of one field
canPutMine(Coord):-
	distanceOne(Dist),
	getAroundFields(Coord, Dist, numericalField, AroundFields),
	nonZeroFields(AroundFields).

% deletes the first element in the list
deleteFirst([Deleted|T], Deleted, T):-
	!.
deleteFirst([H|T], Deleted, [H|Result]):-
	deleteFirst(T, Deleted, Result).

% removes all the elements of the first list from the second list
removeAffectedFields([], Result, Result).
removeAffectedFields([H|T], Affected, Result):-
	debugAssert(member(H, Affected)),
	deleteFirst(Affected, H, NewAffected),
	removeAffectedFields(T, NewAffected, Result),
	!.

% tries to lay mines to the affected fields given in the
% first argument; succeedes when there is no affected field
% for laying mine; LayedMinesFields is for internal purpose 
% and should be called with [] value; the BackDeep is 
% used for backjumping; when this parameter is unified, the
% predicate failed and the BackDeep determines number of mines
% to back jump
checkFieldsForMines([], _, _).
checkFieldsForMines([H|T], LayedMinesFields, BackDeep):-
	debugAssert(numericalField(H)),

	% we will take the first Affected field and get covered fields
	% around it
	distanceOne(Dist),
	getAroundFields(H, Dist, coveredField, CoveredFields),
	(   
	    % we will try to put the mine at each covered field
	    member(MineCoord, CoveredFields),

	    canPutMine(MineCoord),
	    putMine(MineCoord),
	
	    % update the affected field list
	    getAroundFields(MineCoord, Dist, numericalField, NumFields),
	    removeAffectedFields(NumFields, [H|T], AffectedFields),

	    % try to lay next mine
	    checkFieldsForMines(AffectedFields,
		                [MineCoord|LayedMinesFields],
				Deep),

	    pickUpMine(MineCoord),
            (   
	        % the checkFieldsForMines succeeded, if the Deep
	        % is not unified
		var(Deep)
	    ;
	        % check another covered field, if the Deep is zero or
	        % continue up, if there are more mines to remove
		Deep \= 0,
		BackDeep is Deep - 1,
		!
	    )
	;   
	    % there is no way to lay mines to taken affected field,
	    % we will get how many steps we can back jump in recursion
	    findBadRecursionDeep(H, 1, LayedMinesFields, Deep),
	    BackDeep is Deep - 2,
	    !
	).

% tries to lay mines to the given numerical field, until
% the field has zero value
canLayMinesAround(Coord):-zeroField(Coord).
canLayMinesAround(Coord):-
	debugAssert(numericalField(Coord)),
	
	distanceOne(Dist),
	getAroundFields(Coord, Dist, coveredField, CoveredFields),
	member(Covered, CoveredFields),
	canPutMine(Covered),
	putMine(Covered),
	(   
	    canLayMinesAround(Coord),
	    pickUpMine(Covered)
	;   
	    pickUpMine(Covered),
	    fail
	),
	!.

% sequentialy removes layed mines one by one and checks if, there
% is a way, how to lay all needed mines around given numerical field
findBadRecursionDeep(_, CurrentDeep, [], CurrentDeep).
findBadRecursionDeep(AffectedField, CurrentDeep, [H|T], Deep):-
	debugAssert(minedField(H)),
	NewDeep is CurrentDeep + 1,
	pickUpMine(H),
	(   
	    canLayMinesAround(AffectedField),
	    putMine(H),
	    Deep = NewDeep
	;   
	    findBadRecursionDeep(AffectedField, NewDeep, T, Deep),
	    putMine(H)
	),
	!.

distanceOne([(-1,-1), (-1,0), (-1,1),
	     (0,-1), (0,0), (0,1),
	     (1,-1), (1,0), (1,1)]).
distanceTwo([(-2,-2), (-2,-1), (-2,0), (-2,1), (-2,2),
	     (-1,-2), (-1,-1), (-1,0), (-1,1), (-1,2),
	     (0,-2), (0,-1), (0,0), (0,1), (0,2),
	     (1,-2), (1,-1), (1,0), (1,1), (1,2),
	     (2,-2), (2,-1), (2,0), (2,1), (2,2)]).

% get list of all around fields which pass the given condition
getAroundFields(_, [], _, []).
getAroundFields((X, Y), [(DX, DY)|DistanceTail], Condition, [H|T]):-
	CheckedX is X - DX,
	CheckedY is Y - DY,
	
	call(Condition, (CheckedX, CheckedY)),
	H = (CheckedX, CheckedY),
	getAroundFields((X, Y), DistanceTail, Condition, T),
	!.

getAroundFields((X, Y), [_|DistanceTail], Condition, T):-
	getAroundFields((X, Y), DistanceTail, Condition, T).

% adds the fields of the first list to the field of the second list
% without duplicities
addAroundFieldsToAffected([], Affected, Affected):-!.
addAroundFieldsToAffected([H|T], Affected, AddedAffected):-
	debugAssert(numericalField(H)),

	member(H, Affected),
	!,
	addAroundFieldsToAffected(T, Affected, AddedAffected).

addAroundFieldsToAffected([H|T], Affected, AddedAffected):-
	addAroundFieldsToAffected(T, [H|Affected], AddedAffected).

% get affected fields from all the continous area (distance of 
% two fields), which contain is near given coordinate
getAffectedFields(Coord, AffectedFields):-
	distanceTwo(Dist),
	getAroundFields(Coord, Dist, numericalField, AroundFields),
	getAffectedFieldsLow(AffectedFields, AroundFields, []).

getAffectedFieldsLow([],[], _).
getAffectedFieldsLow(AffectedFields, [H|T], Visited):-
	member(H, Visited),
	!,
	getAffectedFieldsLow(AffectedFields, T, Visited).
	
getAffectedFieldsLow([H|AffectedFields], [H|T], Visited):-
	distanceTwo(Dist),
	getAroundFields(H, Dist, numericalField, AroundFields),
	addAroundFieldsToAffected(AroundFields, [H|T], NewA),
	
	getAffectedFieldsLow(AffectedFields, NewA, [H|Visited]).

        
	
	

	









