/*
 * File:		MinesDao.cpp
 * Date:		2. �nora 2008
 * Author:		Tom� Martinec
 * Projekt:		Z�po�tov� program z p�edm�tu Programov�n� I
 * Popis:		Test t��dy pro �e�en� minov�ho pole.
 */

#define _CRT_SECURE_NO_DEPRECATE
#ifdef _DEBUG 
	#define _CRTDBG_MAP_ALLOC
#endif //_DEBUG 

#include <stdlib.h>
#include <crtdbg.h>

#include <stdio.h>
#include <string.h>
#include <tchar.h>

#include "../MinesDao/CMinefield.h"

////////////////////////////////////////////////////////////////////
// error messages
#define STR_OK				"Program uspesne skoncil.\n"
#define STR_MEM_LEAKS		"Detekce memory leaku.\n"
#define STR_OPEN_FILE_FAIL	"Nezdarilo se nacteni souboru.\n"
#define STR_BAD_CONTENT		"Vyskytlo se pole s neznamou hodnotou.\n"
#define STR_BAD_RESULT		"Chyba pri vyhodnocovani: %d, %d. Krok %d.\n"
#define STR_BAD_ARGUMENTS	"Spatny pocet parametru.\n\
		1.Parametr - Zadani\n\
		2.Parametr - Reseni\n\
		3.Parametr - Vystup vysledku -nepovinny\n\
		4.Parametr - Vystup reseni -nepovinny\n"

////////////////////////////////////////////////////////////////////
bool Test(char* szInTaskFile, char* szInResultFile,
		  char* szOutTaskFile, char* szOutResultFile)
{
	CCMinefield TaskField = CCMinefield();
	CCMinefield ResultField = CCMinefield();

	TaskField.Init(szInTaskFile);
	ResultField.Init(szInResultFile);

	if(TaskField.IsEmpty() || ResultField.IsEmpty())
	{
		printf(STR_OPEN_FILE_FAIL);
		return false;
	}

	CItemPool MinedFields = CItemPool(sizeof(COORD));
	CItemPool ClearFields = CItemPool(sizeof(COORD));

	COORD ClearCoord;
	int nStep = 0;
	while(TaskField.Solve(&MinedFields, &ClearFields))
	{
		if(nStep == 8)
		{
//			TaskField.breakp0 = true;
			TaskField.DumpAllFields("Chyba.txt");
		}
		nStep++;
		for(int i = 0; i < (int) ClearFields.GetCount(); i++)
		{
			ClearCoord = *((COORD*)	ClearFields.GetItem(i));
			FIELDVAL ReavVal;
			ResultField.GetField(ClearCoord, &ReavVal);
	//		TaskField.DumpAllFields("Chyba.txt");

			if(ReavVal == FV_MINE )
			{
				// �patn� vyhodnocen�

				TaskField.DumpAllFields(szOutTaskFile);		
				ResultField.DumpAllFields(szOutResultFile);		

				printf(STR_BAD_RESULT, ClearCoord.nX, ClearCoord.nY, nStep);
				ResultField.GetField(ClearCoord, &ReavVal);


				TaskField.DumpAllFields("Chyba.txt");		

				return false;
			}

			if(!TaskField.RevealField(ClearCoord, ReavVal))
			{
						// �patn� vyhodnocen�

						TaskField.DumpAllFields(szOutTaskFile);		
						ResultField.DumpAllFields(szOutResultFile);		

						printf(STR_BAD_RESULT,
						ClearCoord.nX, ClearCoord.nY, nStep);
						TaskField.DumpAllFields();		

						return false;
			}
		}
		ClearFields.Clear();
	}

	TaskField.DumpAllFields(szOutTaskFile);	

	return true;
}

////////////////////////////////////////////////////////////////////
int _tmain(int argc, char* argv[])
{
	char* szInTaskFile;
	char* szInResultFile;
	char* szOutTaskFile;
	char* szOutResultFile;

	switch(argc)
	{
	case 3:
		{
			szInTaskFile = argv[1];
			szInResultFile = argv[2];

			szOutTaskFile = NULL;
			szOutResultFile = NULL;
			break;
		}
	case 5:
		{
			szInTaskFile = argv[1];
			szInResultFile = argv[2];

			szOutTaskFile = argv[3];
			szOutResultFile = argv[4];
			break;
		}
	default:
		{
			printf(STR_BAD_ARGUMENTS);
			return false;
		}
	}

	bool bResult = Test(szInTaskFile, szInResultFile, szOutTaskFile, szOutResultFile);
	if(_CrtDumpMemoryLeaks())
	{
		printf(STR_MEM_LEAKS);
		bResult = false;
	}

	if(bResult)
		printf(STR_OK);

	return bResult;
}

