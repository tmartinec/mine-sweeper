//#include "ItemPool.h"

/*const int E_ITEMPOOL_OK = 0;

////////////////////////////////////////////////////////////////////
class CErrorMan : public CItemPool
{
public:
	CItemPool(unsigned long nItemSize,
		int (__cdecl *compare )(const void *, const void *) = NULL,
		unsigned long nBuff = CITEMPOOL_BUFF); 
	~CItemPool() {if(m_pItems) free(m_pItems);}

	int GetLastError() {return m_nError;}

	unsigned long GetCount() {return m_nItems;}
	unsigned char* GetItem(unsigned long nIndex);
	bool FindItem(unsigned char* pItem, unsigned long* p_nIndex);

	void Sort()
		{if(m_compare) qsort(m_pItems, m_nItems, m_nItemSize, m_compare); }

	bool AddItem(unsigned char* pItem);
	bool SetItem(unsigned char* pItem, unsigned long nIndex);
	void RemoveLast() {if(m_nItems != 0) m_nItems--;}
	void Clear() {m_nItems = 0;}

////////////////////////////////////////////////////////////////////
protected:
	unsigned long m_nItemSize;
	unsigned long m_nBuffSize;
	unsigned long m_nItems;
	unsigned char* m_pItems;

	int (__cdecl *m_compare )(const void *, const void *);

	int m_nError;
};
*/

struct VAR_INFO
{
	char* szVarType;
	char* szVarName;
	void* pVarContent;
	int nVarContentLen;
};

struct STACK_INFO
{
	char* szStackName;
	VAR_INFO* pVars;
	int nVars;
}

////////////////////////////////////////////////////////////////////
class CDebugMan : public CMemMan
{
public:
	CDebugMan() : CMemMan(sizeof(DEBUG_INFO)) {m_nDbgInfo = 0;}
	~CDebugMan();

	int AddStackItem(char* szStackName);
	bool AddVarItem(int nStack, char* szVarType, char* szVarName, int nVarLen);

protected:
	int m_nDbgInfo;

};