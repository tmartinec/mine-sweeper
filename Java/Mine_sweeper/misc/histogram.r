makeHistogram <- function() {
	x <- read.table("D:\\projects\\miny\\trunk\\Java\\Mine_sweeper\\solver_logs\\measuringOut.txt")	
	times <- x$V2 / 1000
	breaks <- seq(from=0, to=70, by=2)
	
	histogram <- hist(times, breaks)

	plot(histogram, col="blue", xlab="Time[s]", ylab="Occurences", 
		main="Times of optimized solver evaluation")

	print(paste("Expected value ", mean(times)))
}