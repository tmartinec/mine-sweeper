@echo off

REM the path must be without spaces   
set MINESWEEPER_SOURCES_PATH=D:\projects\miny\trunk\Java\Mine_sweeper

set OLD_PATH=%PATH%
set OLD_PLUGIN_PATH=%QT_PLUGIN_PATH%
set OLD_CLASSPATH=%CLASSPATH%

set QT_PLUGIN_PATH=%cd%\plugins
set PATH=%cd%\bin

dir /b qtjambi-4*.jar > .tmp
set /p QTJAMBI_JAR=< .tmp
del .tmp
set VERSION=%QTJAMBI_JAR:~8,8%

set CLASSPATH=%cd%\qtjambi-%VERSION%.jar;%cd%\qtjambi-designer-%VERSION%.jar

REM here we adds library, which contains custom mines widgets;

set CLASSPATH=%CLASSPATH%;%MINESWEEPER_SOURCES_PATH%\designer_temp\Mine_sweeper.jar
echo %CLASSPATH%

set DESIGNER_BINARY="%cd%\bin\designer"
cd %MINESWEEPER_SOURCES_PATH%\src\mines\gui\jui 

%DESIGNER_BINARY%

set PATH=%OLD_PATH%
set QT_PLUGIN_PATH=%OLD_PLUGIN_PATH%
set CLASSPATH=%OLD_CLASSPATH%

set OLD_PATH=
set OLD_PLUGIN_PATH=
set OLD_CLASSPATH=
set QTJAMBI_JAR=
