/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package mines;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import junit.framework.Assert;
import mines.MinefieldContainer.Coord;
import org.junit.Test;

/**
 *
 * @author tomas
 */
public class MinesUtilsTest {

    public MinesUtilsTest() {
    }

	@Test
	public void testGetAllFieldsCoords() {
		List<Coord> coords = MinesUtils.getAllFieldsCoords(3);
		Set<Coord> real = new HashSet<Coord>(coords);

		Assert.assertEquals(9, real.size());
	}

	@Test
	public void testIsDownButton() {
		Assert.assertTrue(MinesUtils.isDownButton(0));
		Assert.assertFalse(MinesUtils.isDownButton(1));
		Assert.assertFalse(MinesUtils.isDownButton(3));
	}

	@Test
	public void testGetFieldsCount() {
		Assert.assertEquals(25, MinesUtils.getFieldsCount(5));
	}

//	@Test
//	public void testUpdateFieldsNumbers() {
//		MinefieldContainer<BUTTON_TYPE> instance = new MinefieldContainer<BUTTON_TYPE>(3);
//		instance.initialFill(BUTTON_TYPE.ZERO);
//
//		instance.set(new Coord(0,0), BUTTON_TYPE.MINE);
//		instance.set(new Coord(1,1), BUTTON_TYPE.MINE);
//
//		MinesUtils.updateFieldsNumbers(instance);
//
//		MinefieldContainer<BUTTON_TYPE> expected = new MinefieldContainer<BUTTON_TYPE>(3);
//		expected.set(new Coord(0,0), BUTTON_TYPE.MINE);
//		expected.set(new Coord(0,1), BUTTON_TYPE.TWO);
//		expected.set(new Coord(0,2), BUTTON_TYPE.ONE);
//		expected.set(new Coord(1,0), BUTTON_TYPE.TWO);
//		expected.set(new Coord(1,1), BUTTON_TYPE.MINE);
//		expected.set(new Coord(2,0), BUTTON_TYPE.TWO);
//		expected.set(new Coord(2,1), BUTTON_TYPE.ONE);
//		expected.set(new Coord(3,0), BUTTON_TYPE.ONE);
//		expected.set(new Coord(4,0), BUTTON_TYPE.ZERO);
//
//		Assert.assertEquals(expected, instance);
//	}

//	@Test
//	public void testGetAffectedFields1() {
//		MinefieldContainer<MineSolver.SOLVER_FIELD> data =
//				TestSuite.loadSolverData("./test/mines/testdata/getAffectedFields1.msd");
//
//		List<Coord> affectedFields = MinesUtils.getAffectedFields(data, new Coord(0,1));
//
//		List<Coord> expected = new ArrayList<Coord>();
//		expected.add(new Coord(0,0));
//
//		TestSuite.assertListsEquals(expected, affectedFields);
//	}
//
//	@Test
//	public void testGetAffectedFields2() {
//		MinefieldContainer<MineSolver.SOLVER_FIELD> data =
//				TestSuite.loadSolverData("./test/mines/testdata/getAffectedFields1.msd");
//
//		List<Coord> affectedFields = MinesUtils.getAffectedFields(data, new Coord(11,0));
//		Set<Coord> real = new HashSet<Coord>(affectedFields);
//
//		Assert.assertEquals(affectedFields.size(), real.size());
//
//		Set<Coord> expected = new HashSet<Coord>();
//		expected.add(new Coord(12,0));
//		expected.add(new Coord(8,0));
//		expected.add(new Coord(8,2));
//		expected.add(new Coord(4,2));
//		expected.add(new Coord(4,4));
//		expected.add(new Coord(1,4));
//		expected.add(new Coord(0,4));
//
//		TestSuite.assertSetsEquals(expected, real);
//	}
//
//	@Test
//	public void testGetAffectedFields3() {
//		MinefieldContainer<MineSolver.SOLVER_FIELD> data =
//				TestSuite.loadSolverData("./test/mines/testdata/getAffectedFields2.msd");
//
//		List<Coord> affectedFields = MinesUtils.getAffectedFields(data, new Coord(0,6));
//		Set<Coord> real = new HashSet<Coord>(affectedFields);
//
//		Assert.assertEquals(affectedFields.size(), real.size());
//
//		Set<Coord> expected = new HashSet<Coord>();
//		expected.add(new Coord(0,5));
//		expected.add(new Coord(1,5));
//		expected.add(new Coord(2,5));
//		expected.add(new Coord(3,4));
//		expected.add(new Coord(2,4));
//		expected.add(new Coord(3,3));
//		expected.add(new Coord(4,3));
//		expected.add(new Coord(5,3));
//		expected.add(new Coord(6,3));
//		expected.add(new Coord(7,3));
//		expected.add(new Coord(7,2));
//		expected.add(new Coord(8,2));
//		expected.add(new Coord(9,1));
//		expected.add(new Coord(10,1));
//		expected.add(new Coord(11,1));
//		expected.add(new Coord(12,1));
//		expected.add(new Coord(13,1));
//		expected.add(new Coord(12,2));
//		expected.add(new Coord(11,2));
//		expected.add(new Coord(10,3));
//		expected.add(new Coord(9,3));
//		expected.add(new Coord(8,4));
//		expected.add(new Coord(9,4));
//		expected.add(new Coord(10,4));
//
//		TestSuite.assertSetsEquals(expected, real);
//	}
}