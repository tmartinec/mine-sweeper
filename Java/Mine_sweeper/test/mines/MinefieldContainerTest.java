package mines;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import java.util.Set;
import junit.framework.Assert;

import mines.MinefieldContainer.Coord;

import org.junit.Test;

public class MinefieldContainerTest {

	@Test
	public void testGetAroundFields1() {
		MinefieldContainer<GUI_FIELD> instance = new MinefieldContainer<GUI_FIELD>(1);
		try {
			instance.getAroundFields(0, -1);
		} catch(Error e) {
			return;
		}
			
		fail("Should not get here");
	}
	
	@Test
	public void testGetAroundFields2() {
		MinefieldContainer<GUI_FIELD> instance = new MinefieldContainer<GUI_FIELD>(1);

		List<Coord> coords = instance.getAroundFields(0, 0);
		
		List<Coord> expectedCoords = new ArrayList<Coord>();
		expectedCoords.add(new Coord(0,0));
		
		TestSuite.assertListsEquals(expectedCoords, coords);
	}

	@Test
	public void testGetAroundFields3() {
		MinefieldContainer<GUI_FIELD> instance = new MinefieldContainer<GUI_FIELD>(3);

		List<Coord> coords = instance.getAroundFields(2, 0);
		Set<Coord> real = new HashSet<Coord>(coords);

		Assert.assertEquals(coords.size(), real.size());

		Set<Coord> expectedCoords = new HashSet<Coord>();
		expectedCoords.add(new Coord(0,0));
		expectedCoords.add(new Coord(0,1));
		expectedCoords.add(new Coord(1,0));
		expectedCoords.add(new Coord(1,1));
		expectedCoords.add(new Coord(2,0));
		expectedCoords.add(new Coord(2,1));
		expectedCoords.add(new Coord(3,0));
		expectedCoords.add(new Coord(4,0));
		
		TestSuite.assertSetsEquals(expectedCoords, real);
	}

	@Test
	public void testgetAllFieldsIndexes() {
		MinefieldContainer<GUI_FIELD> instance = new MinefieldContainer<GUI_FIELD>(3);
		instance.initialFill(GUI_FIELD.ZERO);

		List<Coord> coords = instance.getAllFieldsIndexes(GUI_FIELD.ALL_FLAGS);
		Set<Coord> real = new HashSet<Coord>(coords);

		Assert.assertEquals(9, real.size());
	}
}
