/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package mines;

import general.IFilterAble;
import java.util.List;
import java.util.Set;
import junit.framework.Assert;
import mines.MinesUtils.MineSweeperData;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * @author tomas
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({mines.MinefieldContainerTest.class, mines.MinesUtilsTest.class})
public class TestSuite {

	@BeforeClass
	public static void setUpClass() throws Exception {
	}

	@AfterClass
	public static void tearDownClass() throws Exception {
	}

//	public static MinefieldContainer<SOLVER_FIELD> loadSolverData(String path) {
//		 MineSweeperData sweeperData = MinesUtils.<MineSweeperData>loadObjectFromFileSafely(path);
//
//		 return MinesUtils.convertGUIToSolverMinefield(sweeperData.mViewedContent);
//	}

	public static <T> void assertListsEquals(List<T> expected, List<T> real) {
		Assert.assertEquals(expected.size(), real.size());

		for (int i = 0; i < expected.size(); i++) {
			T expectedMember = expected.get(i);
			T realMember = real.get(i);

			Assert.assertEquals(expectedMember, realMember);
		}
	}

	public static <T> void assertSetsEquals(Set<T> expected, Set<T> real) {
		Assert.assertEquals(expected.size(), real.size());

		for (T expectedMember : expected) {
			Assert.assertTrue(real.contains(expectedMember));
		}
	}
}