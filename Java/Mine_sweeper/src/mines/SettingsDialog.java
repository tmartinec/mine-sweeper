/**
 * Copyright (C) 2010, LGPL
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * @author Matfyz, fyzmat@gmail.com
 */

package mines;

import mines.gui.generated.Ui_SettingsDialog;

import com.trolltech.qt.core.Qt;
import com.trolltech.qt.gui.*;

/**
 * Controller of the settings dialog.
 */
public class SettingsDialog extends QDialog {

	/**
	 * Data, which can be set in the dialog.
	 */
	public static class SettingsData {
		/** Size of minefield */
		public int mMinefieldSize = MineSweeperLogic.DEFAULT_MINEFIELD_SIZE;

		/** Count of mines in minefield */
		public int mMinesCount = MineSweeperLogic.DEFAULT_MINES;

		/** If the coordinates are visible in the main window */
		public boolean mCoordinatesVisible = false;

		/** Default constructor */
		public SettingsData() {}

		/** Copy constructor */
		public SettingsData(SettingsData data) {
			this.mCoordinatesVisible = data.mCoordinatesVisible;
			this.mMinefieldSize = data.mMinefieldSize;
			this.mMinesCount = data.mMinesCount;
		}
	}

	/** GUI for the settings dialog */
    Ui_SettingsDialog ui = new Ui_SettingsDialog();

	/** Settings of the mine sweeper */
	SettingsData mData = new SettingsData();

	/** Signals, that the user closed the dialog and wants to perform the changes. */
	public Signal1<SettingsData> mAcceptedSignal = new Signal1<SettingsData>();

	/**
	 * Initializes the GUI and connects it with logic.
	 * @param parent Parent window.
	 */
    public SettingsDialog(QWidget parent) {
        super(parent, Qt.WindowType.WindowSystemMenuHint);
        ui.setupUi(this);
                
        ui.mMinefieldSizeLineEdit.editingFinished.connect(this, "sizeChanged()");
        ui.mMinesCountLineEdit.editingFinished.connect(this, "minesChanged()");
		ui.mCoordinatesCheckbox.toggled.connect(this, "coordinatesBoxChanged(boolean)");

        ui.mButtonBox.accepted.connect(this, "accept()");
        ui.mButtonBox.rejected.connect(this, "reject()");
        
		this.accepted.connect(this, "onAccepted()");

        setParams(mData);
    }

	/**
	 * Opens this dialog and sets the given data in it.
	 * @param data Data to be set in the dialog.
	 */
	void runDialog(SettingsData data) {
		setParams(data);
		exec();		
	}

	/**
	 * Emits the accepted signal.
	 */
	void onAccepted() {
		mAcceptedSignal.emit(new SettingsData(mData));
	}

	/**
	 * Sets validators for the line edits in gui.
	 */
    void setValidators() {
    	QValidator sizeValidator = new QIntValidator(1, MineSweeperLogic.MAXIMUM_MINEFIELD_SIZE, this);
    	
    	int fields = MinesUtils.getFieldsCount(mData.mMinefieldSize);
    	QValidator minesValidator = new QIntValidator(0, fields, this);
    	
    	ui.mMinefieldSizeLineEdit.setValidator(sizeValidator);
    	ui.mMinesCountLineEdit.setValidator(minesValidator);
    }

	/**
	 * Signal handler, when the user has changed the size of minefield.
	 */
    void sizeChanged() {
    	mData.mMinefieldSize = Integer.valueOf(ui.mMinefieldSizeLineEdit.text());

    	int fieldsCount = MinesUtils.getFieldsCount(mData.mMinefieldSize);
    	mData.mMinesCount = (mData.mMinesCount > fieldsCount) ? fieldsCount : mData.mMinesCount;
    	
    	setParams(mData);
    }

	/**
	 * Signal handler, when the user has changed the count of mines.
	 */
    void minesChanged() {
    	mData.mMinesCount = Integer.valueOf(ui.mMinesCountLineEdit.text());
    }

	/**
	 * Signal handler, when the user has changed the state of the coordinates
	 * checkbox.
	 */
	void coordinatesBoxChanged(boolean checked) {
		mData.mCoordinatesVisible = checked;
	}

	/**
	 * Sets content of this dialog.
	 * @param data Data to be set.
	 */
    void setParams(SettingsData data) {
    	mData = new SettingsData(data);

		ui.mCoordinatesCheckbox.setChecked(mData.mCoordinatesVisible);
    	ui.mMinefieldSizeLineEdit.setText("" + mData.mMinefieldSize);
        ui.mMinesCountLineEdit.setText("" + mData.mMinesCount);

    	setValidators();    	
    }
}
