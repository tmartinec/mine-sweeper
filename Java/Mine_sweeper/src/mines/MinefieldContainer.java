/**
 * Copyright (C) 2010, LGPL
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * @author Matfyz, fyzmat@gmail.com
 */

package mines;

import general.IFilterAble;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <P>Data structure for minefield representation. It is 2 dimensional jagged
 * array indexed by two integer indexes. The first one is index of row and the
 * second one is index of column. The count of columns can differ for each row.</P>
 *
 * <P>TODO: This container is able to be stored to a file because of the serialization
 * mechanism. Currently the default serialization is used. This makes troubles, when
 * some changes in class structures are made, so it is handy to make the
 * serialization independent from class structure.</P>
 *
 * @param <T> Type stored in this container.
 */
public class MinefieldContainer<T extends IFilterAble> implements Serializable {

	private static final long serialVersionUID = 5409066451883353578L;

	/**
	 * Simple class for 2 dimensional coordinated. The first dimension is for
	 * rows and the second for columns.
	 */
	public static class Coord {
		/** Index of a row */
		public int mX;
		/** Index of a column */
		public int mY;

		/**
		 * Initialize the coordinate with given values.
		 * @param x Index of a row.
		 * @param y Index of a column.
		 */
		public Coord(int x, int y) {
			mX = x;
			mY = y;
		}

		/**
		 * Multiply the coordinates by -1.
		 * @return Inverted coordinates.
		 */
		public Coord getInverted() {
			return new Coord(mX * -1, mY * -1);
		}

		/**
		 * Just because the equals is overriden.
		 * @return The hash code.
		 */
		public int hashCode() {
			return (new Integer(mX)).hashCode() * (new Integer(mY)).hashCode();
		}

		/**
		 * Finds out, if the content of the given object is equal with this
		 * @param o Object to be compared.
		 * @return True, if the given object has same type and the coordinates are
		 * same.
		 */
		@Override
		public boolean equals(Object o) {
	        if(!(o instanceof Coord)) {
	            return false;
	        }

	        Coord c = (Coord) o;
	        return (mX == c.mX) && (mY == c.mY);
		}

		/**
		 * Gets the string representation of the coordinates.
		 * @return Coordinates in a form (x;y).
		 */
		@Override
		public String toString() {
			return "(" + mX + ";" + mY + ")";
		}

		/**
		 * Makes addition of two coordinates together.
		 * @param coord1 First coordinates to add.
		 * @param coord2 Second coordinates to add.
		 * @return (coord1.x + coord2.x; coord1.y + coord2.y)
		 */
		public static Coord Sum(Coord coord1, Coord coord2) {
			int x = coord1.mX + coord2.mX;
			int y = coord1.mY + coord2.mY;

			return new Coord(x, y);
		}
	}

	/**
	 * Container for columns.
	 */
	private class Row implements Serializable{
		private static final long serialVersionUID = 5360446350024068158L;

		/** List of columns. */
		public ArrayList<T> mColumns = null;

		/**
		 * Allocate the row for given number of columns.
		 * @param columns Count of columns, which will by contained by this row.
		 */
		Row(int columns) {
			mColumns = new ArrayList<T>(columns);

			for(int i = 0; i < columns; i++) {
				mColumns.add(null);
			}
		}

		/**
		 * Copy constructor.
		 * @param original The original of the copy.
		 */
		Row(Row original) {
			mColumns = new ArrayList<T>();

			for (int i = 0; i < original.mColumns.size(); i++) {
				mColumns.add(original.mColumns.get(i));
			}
		}

		/**
		 * Finds out, if the content of the given object equals to the content
		 * of this row.
		 * @param o Object to compare.
		 * @return True, if the given object has same type and each appropriate columns
		 * of both rows equals.
		 */
		@Override
		public boolean equals(Object o) {
			if (this.getClass() != o.getClass()) {
				return false;
			}

			Row compared = (Row) o;

			// check the size

			if (this.mColumns.size() != compared.mColumns.size()) {
				return false;
			}
			
			// check each pair of the columns
			for (int i = 0; i < this.mColumns.size(); i++) {
				T thisColumn = mColumns.get(i);
				T comparedColumn = compared.mColumns.get(i);

				if(!thisColumn.equals(comparedColumn)) {
					return false;
				}
			}

			return true;			
		}

		/**
		 * Implemented just because of the overriding the equals method.
		 * @return The hash code
		 */
		@Override
		public int hashCode() {
			int hash = 7;
			hash = 31 * hash + (this.mColumns != null ? this.mColumns.hashCode() : 0);
			return hash;
		}
	}

	/**
	 * Enumeration of distances, how far the around fields can be thought.
	 */
	public enum AROUND_FIELDS_DISTANCE {
		ZERO,
		ONE,
		TWO
	}

	/**
	 * Map of indexes of around fields indexed by the distance of the around fields.
	 * This contains the differences of the around fields coordinates from the 
	 * coordinates of the centered field, which has up orientation
	 */
	static final Map<AROUND_FIELDS_DISTANCE, List<Coord>> mAroundFieldsIndexes = loadAroundFieldIndexes();	

	/**
	 * Initializes the map of indexes of around fields.
	 * @return The initialized map.
	 */
	static Map<AROUND_FIELDS_DISTANCE, List<Coord>> loadAroundFieldIndexes() {
		Map<AROUND_FIELDS_DISTANCE, List<Coord>> result = new HashMap<AROUND_FIELDS_DISTANCE, List<Coord>>();

		// the difference of the coordinates of around fields from the coordinates
		// of centered field depends on the orientation of the centered field
		
		// these differences are from the UP orientation

		// initialize the centered field coordinates
		List<Coord> zeroCoords = new ArrayList<Coord>();
		zeroCoords.add(new Coord(0, 0));
		
		result.put(AROUND_FIELDS_DISTANCE.ZERO, zeroCoords);

		// initialize the coordinates of fields with distance one
		List<Coord> oneCoords = new ArrayList<Coord>();
		oneCoords.add(new Coord(-3, 1));
		oneCoords.add(new Coord(-2, 0));
		oneCoords.add(new Coord(-2, 1));
		oneCoords.add(new Coord(-1, 0));
		oneCoords.add(new Coord(-1, 1));
		oneCoords.add(new Coord(0, -1));
		oneCoords.add(new Coord(0, 0));
		oneCoords.add(new Coord(0, 1));
		oneCoords.add(new Coord(1, -1));
		oneCoords.add(new Coord(1, 0));
		oneCoords.add(new Coord(1, 1));
		oneCoords.add(new Coord(2, -1));
		oneCoords.add(new Coord(2, 0));
		
		result.put(AROUND_FIELDS_DISTANCE.ONE, oneCoords);

		// initialize the coordinates of fields with distance two
		List<Coord> twoCoords = new ArrayList<Coord>();
		twoCoords.add(new Coord(-5, 1));
		twoCoords.add(new Coord(-5, 2));
		twoCoords.add(new Coord(-4, 0));
		twoCoords.add(new Coord(-4, 1));
		twoCoords.add(new Coord(-4, 2));
		twoCoords.add(new Coord(-3, 0));
		twoCoords.add(new Coord(-3, 1));
		twoCoords.add(new Coord(-3, 2));
		twoCoords.add(new Coord(-2, -1));
		twoCoords.add(new Coord(-2, 0));
		twoCoords.add(new Coord(-2, 1));
		twoCoords.add(new Coord(-2, 2));
		twoCoords.add(new Coord(-1, -1));
		twoCoords.add(new Coord(-1, 0));
		twoCoords.add(new Coord(-1, 1));
		twoCoords.add(new Coord(-1, 2));
		twoCoords.add(new Coord(0, -2));
		twoCoords.add(new Coord(0, -1));
		twoCoords.add(new Coord(0, 0));
		twoCoords.add(new Coord(0, 1));
		twoCoords.add(new Coord(0, 2));
		twoCoords.add(new Coord(1, -2));
		twoCoords.add(new Coord(1, -1));
		twoCoords.add(new Coord(1, 0));
		twoCoords.add(new Coord(1, 1));
		twoCoords.add(new Coord(1, 2));
		twoCoords.add(new Coord(2, -2));
		twoCoords.add(new Coord(2, -1));
		twoCoords.add(new Coord(2, 0));
		twoCoords.add(new Coord(2, 1));
		twoCoords.add(new Coord(3, -2));
		twoCoords.add(new Coord(3, -1));
		twoCoords.add(new Coord(3, 0));
		twoCoords.add(new Coord(3, 1));
		twoCoords.add(new Coord(4, -2));
		twoCoords.add(new Coord(4, -1));
		twoCoords.add(new Coord(4, 0));
		
		result.put(AROUND_FIELDS_DISTANCE.TWO, twoCoords);
		
		return result;
	}

	/** The rows of the minefield */
	ArrayList<Row> mRows = null;
	/** The size of the minefield */
	int mSize = 0;

	/**
	 * Allocates the minefield for given size.
	 * @param size Size of the minefield.
	 */
	public MinefieldContainer(int size) {
		int rowsCount = (size * 2) - 1;
		
		mSize = size;
		mRows = new ArrayList<Row>(0);

		for (int i = 0; i < rowsCount; i++) {
			int baseColumns = size - (i / 2);
			int additionalColumns = (i % 2 == 0) ? 0 : -1;
			
			int columns = baseColumns + additionalColumns;
			
			Row row = new Row(columns);
			mRows.add(row);
		}
	}

	/**
	 * Copy constructor.
	 * @param original Original minefield.
	 */
	public MinefieldContainer(MinefieldContainer<T> original) {
		mSize = original.mSize;
		
		mRows = new ArrayList<Row>(0);

		for (Row copyRow : original.mRows) {
			Row row = new Row(copyRow);
			mRows.add(row);
		}
	}

	/**
	 * Finds out, if the given object has the same content as this object.
	 * @param o Object to be compared.
	 * @return True, if the given object has same type, same size and every appropriate
	 * pair of fields equals.
	 */
	@Override
	public boolean equals(Object o) {
		if (this.getClass() != o.getClass()) {
			return false;
		}

		@SuppressWarnings("unchecked")
		MinefieldContainer<T> compared = (MinefieldContainer<T>) o;

		if (this.mSize != compared.mSize) {
			return false;
		}

		// check all the columns in all the rows
		for (int i = 0; i < mRows.size(); i++) {
			Row thisRow = mRows.get(i);
			Row comparedRow = compared.mRows.get(i);
			
			if(!thisRow.equals(comparedRow)) {
				return false;
			}
		}
		
		return true;
	}

	/**
	 * Just because of overriding the equals.
	 * @return The hash code.
	 */
	@Override
	public int hashCode() {
		int hash = 5;
		hash = 79 * hash + (this.mRows != null ? this.mRows.hashCode() : 0);
		hash = 79 * hash + this.mSize;
		return hash;
	}

	/**
	 * Sets specified value to specified field.
	 * @param coord Coordinates of the field to be set.
	 * @param value Value to be set.
	 */
	public void set(Coord coord, T value) {
		set(coord.mX, coord.mY, value);
	}

	/**
	 * Sets specified value to specified field.
	 * @param row_index Index of the row of the field to be set.
	 * @param column_index Index of the column of the field to be set.
	 * @param value Value to be set.
	 */
	public void set(int row_index, int column_index, T value) {
		if(!isValidIndex(row_index, column_index)) {
			throw new Error("Internal error, invalid index " + row_index + " " + column_index);
		}

		Row row = mRows.get(row_index);
		row.mColumns.set(column_index, value);
	}

	/**
	 * Sets the content of all the fields to the specified value.
	 * @param value Value to be set.
	 */
	public void initialFill(T value) {
		for (Row row : mRows) {
			int columnsCount = row.mColumns.size();
			
			for (int i = 0; i < columnsCount; i++) {
				row.mColumns.set(i, value);
			}
		}
	}

	/**
	 * Gets the value of the specified field.
	 * @param coord Coordinates of the field.
	 * @return Value of the field.
	 */
	public T get(Coord coord) {
		return get(coord.mX, coord.mY);
	}

	/**
	 * Gets the value of the specified field.
	 * @param row_index Index of the row of the field.
	 * @param column_index Index of the column of the field.
	 * @return Value of the field.
	 */
	public T get(int row_index, int column_index) {
		if(!isValidIndex(row_index, column_index)) {
			throw new Error("Internal error, invalid index " + row_index + " " + column_index);
		}
		
		T result = mRows.get(row_index).mColumns.get(column_index);
		
		if (result == null) {
			throw new Error("Internal error, uninitialized minefield content " + row_index + " " + column_index);
		}
		
		return result;
	}

	/**
	 * Checks, if the index is valid for the current minefield.
	 * @param row_index Index of a row.
	 * @param column_index Index of a column.
	 * @return True, if the index is not out of bounds.
	 */
	boolean isValidIndex(int row_index, int column_index) {
		if ((row_index < 0) || (row_index >= mRows.size())) {
			return false;
		}
		
		Row row = mRows.get(row_index);

		if ((column_index < 0) || (column_index >= row.mColumns.size())) {
			return false;
		}
		
		return true;	
	}	

	/**
	 * Option for {@link #getAroundFields} method.
	 */
	public enum CENTER_OPTION {
		INCLUDE_CENTER,
		EXCLUDE_CENTER
	}

	/**
	 * Gets the coordinates of all the fields which are in neighbourhood of the specified
	 * field. The neighbourhood has distance one from the centered field. The center
	 * coordinates are included.
	 * 
	 * @param coord Coordinates of the centered field.
	 * @return List of coordinates of the fields in the neighbourhood.
	 */
	public List<Coord> getAroundFields(Coord coord) {
		return getAroundFields(coord.mX, coord.mY);
	}

	/**
	 * Gets the coordinates of all the fields which are in neighbourhood of the specified
	 * field. The neighbourhood has distance one from the centered field. The center
	 * coordinates are included.
	 *
	 * @param row_index Index of the row of the center field.
	 * @param column_index Index of the column of the center field.
	 * @return List of coordinates of the fields in the neighbourhood.
	 */
	public List<Coord> getAroundFields(int row_index, int column_index) {
		return getAroundFields(row_index, column_index,	AROUND_FIELDS_DISTANCE.ONE,
				CENTER_OPTION.INCLUDE_CENTER);
	}	

	/**
	 * Gets the coordinates of the selected fields which are in neighbourhood of the specified
	 * field. The neighbourhood can be specified and the coordinates can be selected
	 * by the filter.
	 *
	 * @param row_index Index of the row of the center field.
	 * @param column_index Index of the column of the center field.
	 * @param distance Distance of the around fields from the centered field.
	 * @param filter Filter flags to make the selection of the around coordinates.
	 * The result contains all the coordinates in the neighbourhood, which were
	 * filtered by the given filter.
	 * @param center Option whether to include the centered field to the result
	 * of whether not.
	 * @return List of coordinates of the fields in the neighbourhood.
	 */
	public List<Coord> getAroundFields(int row_index, int column_index, AROUND_FIELDS_DISTANCE distance,
			long filter, CENTER_OPTION center) {

		// get the unfiltered coordinates of the around fields
		List<Coord> aroundFields = getAroundFields(row_index, column_index, distance, center);

		// filter the coordinates to result
		List<Coord> result = new ArrayList<Coord>();
		for(Coord aroundField : aroundFields) {
			T value = get(aroundField.mX, aroundField.mY);
			
			if (value.isFiltered(filter)) {
				result.add(aroundField);
			}
		}
		
		return result;
	}

	/**
	 * Gets the coordinates of the selected fields which are in neighbourhood of the specified
	 * field. The neighbourhood has distance one from the centered field and the
	 * coordinates can be selected by the filter. The center coordinates are included.
	 *
	 * @param coord Coordinates of the centered field.
	 * @param filter Filter flags to make the selection of the around coordinates.
	 * The result contains all the coordinates in the neighbourhood, which were
	 * filtered by the given filter.
	 * @return List of coordinates of the fields in the neighbourhood.
	 */
	public List<Coord> getAroundFields(Coord coord, long filter) {
		
		return getAroundFields(coord.mX, coord.mY, AROUND_FIELDS_DISTANCE.ONE, filter,
				CENTER_OPTION.INCLUDE_CENTER);
	}

	/**
	 * Gets the coordinates of the selected fields which are in neighbourhood of the specified
	 * field. The neighbourhood can be specified and the coordinates can be selected
	 * by the filter.
	 *
	 * @param coord Coordinates of the centered field.
	 * @param distance Distance of the around fields from the centered field.
	 * @param filter Filter flags to make the selection of the around coordinates.
	 * The result contains all the coordinates in the neighbourhood, which were
	 * filtered by the given filter.
	 * @param center Option whether to include the centered field to the result
	 * of whether not.
	 * @return List of coordinates of the fields in the neighbourhood.
	 */
	public List<Coord> getAroundFields(Coord coord, AROUND_FIELDS_DISTANCE distance,
			long filter, CENTER_OPTION center) {
		
		return getAroundFields(coord.mX, coord.mY, distance, filter, center);
	}

	/**
	 * Gets the coordinates of the selected fields which are in neighbourhood of the specified
	 * field. The neighbourhood can be specified and the coordinates can be selected
	 * by the filter. The center coordinates are included.
	 *
	 * @param row_index Index of the row of the center field.
	 * @param column_index Index of the column of the center field.
	 * @param distance Distance of the around fields from the centered field.
	 * @param filter Filter flags to make the selection of the around coordinates.
	 * The result contains all the coordinates in the neighbourhood, which were
	 * filtered by the given filter.
	 * @return List of coordinates of the fields in the neighbourhood.
	 */
	public List<Coord> getAroundFields(int row_index, int column_index,
			AROUND_FIELDS_DISTANCE distance, long filter) {

		return getAroundFields(row_index, column_index, distance, filter,
				CENTER_OPTION.INCLUDE_CENTER);
	}

	/**
	 * Gets the coordinates of fields which are in neighbourhood of the specified
	 * field. The neighbourhood can be specified.
	 *
	 * @param row_index Index of the row of the centered field.
	 * @param column_index Index of the column of the centered field.
	 * @param distance Distance of the around fields from the centered field.
	 * @param center Option whether to include the centered field to the result
	 * of whether not.
	 * @return List of coordinates of the fields in the neighbourhood.
	 */
	public List<Coord> getAroundFields(int row_index, int column_index,
			AROUND_FIELDS_DISTANCE distance, CENTER_OPTION center) {

		if(!isValidIndex(row_index, column_index)) {
			throw new Error("Internal error, invalid index " + row_index + " " + column_index);
		}

		// we will get the coordinates of the centered field
		// and the differences of the coordinates of the around fields

		List<Coord> result = new ArrayList<Coord>();
		Coord centerCoord = new Coord(row_index, column_index);

		// we must make an indexes copy here
		List<Coord> aroundIndexesFromMap = mAroundFieldsIndexes.get(distance);
		List<Coord> aroundIndexesCopy = new ArrayList<Coord>(aroundIndexesFromMap);

		if (center.equals(CENTER_OPTION.EXCLUDE_CENTER)) {
			aroundIndexesCopy.remove(new Coord(0,0));
		}

		// we will add the difference coordinates to the centered coordinates
		for(Coord aroundCoord : aroundIndexesCopy) {
			// if the centered field has down coordinates, we have to invert
			// the difference coordinates
			if(MinesUtils.isDownButton(centerCoord.mX)) {
				aroundCoord = aroundCoord.getInverted();
			}
			
			Coord fieldCoord = Coord.Sum(centerCoord, aroundCoord);

			// we will ignore the coordinates, which are out of bounds of this
			// minefield
			if(isValidIndex(fieldCoord.mX, fieldCoord.mY)) {
				result.add(fieldCoord);
			}
		}
		
		return result;
	}	

	/**
	 * Get all of the coordinates of fields, which are filtered by the given
	 * filter.
	 *
	 * @param filter Filter flags to make the selection of coordinates.
	 * @return List of selected coordinated.
	 */
	public List<Coord> getAllFieldsIndexes(long filter) {
		List<Coord> result = new ArrayList<Coord>();
		
		for (int i = 0; i < mRows.size(); i++) {
			Row row = mRows.get(i);
			
			for(int j = 0; j < row.mColumns.size(); j++) {
				T field = row.mColumns.get(j);
				
				if (field.isFiltered(filter)) {
					Coord index = new Coord(i, j);
					result.add(index);
				}
			}
		}
		
		return result;
	}

	/**
	 * Gets the size of this minefield.
	 * @return Size of this minefield.
	 */
	public int getSize() {
		return mSize;
	}
}
