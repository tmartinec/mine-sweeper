/**
 * Copyright (C) 2010, LGPL
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * @author Matfyz, fyzmat@gmail.com
 */

package mines;

import com.trolltech.qt.gui.QWidget;
import mines.gui.generated.Ui_AboutDialog;

/**
 * The about dialog, all things are static
 */
public class AboutDialog extends QWidget{

	/** GUI for the about dialog */
	Ui_AboutDialog ui = new Ui_AboutDialog();

	/**
	 * Initializes the gui
	 * @param parent The parent window
	 */
	public AboutDialog(QWidget parent) {
		super(parent);
		ui.setupUi(this);
	}
}
