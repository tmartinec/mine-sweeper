/**
 * Copyright (C) 2010, LGPL
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * @author Matfyz, fyzmat@gmail.com
 */

package mines;

import com.trolltech.qt.gui.QListWidgetItem;
import com.trolltech.qt.gui.QWidget;
import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.List;
import mines.gui.generated.Ui_MinefieldDebugDataViewer;

/**
 * Controller of the GUI window, which is supposed to work as a viewer of
 * debugging data.
 */
public class MinefieldDebugDataViewer extends QWidget{

	/**
	 * Filter for debugging data filenames.
	 */
	private class DebugDataFilenameFilter implements FilenameFilter {

		/**
		 * Accepts only the files with MineSolver.MINESOLVER_DATA_SUFFIX suffix.
		 * @param dir Ignored
		 * @param name Filename of the filtered file.
		 * @return True, if this is file with debugging data.
		 */
		public boolean accept(File dir, String name) {
			return name.endsWith(MineSolver.MINESOLVER_DATA_SUFFIX);
		}
	}

	/** GUI for this window */
	Ui_MinefieldDebugDataViewer ui = new Ui_MinefieldDebugDataViewer();

	/**
	 * Initializes the GUI and connects it with logic.
	 * @param parent Parent window.
	 */
	public MinefieldDebugDataViewer(QWidget parent) {
		super(parent);
		ui.setupUi(this);

		ui.mRefreshButton.clicked.connect(this, "fillDebugFilesList()");
		ui.mCloseButton.clicked.connect(this, "closeButtonClicked()");
		ui.mMinefieldsList.itemClicked.connect(this, "showDebugData(QListWidgetItem)");
		ui.mMinefieldView.connectToButtonsMouseOver(ui.mCoordinatesPanel, "showCoordinates(MineButton)");

		ui.mDirectoryEdit.setText(MineSolver.MINESOLVER_DATA_DIR);
		fillDebugFilesList();
	}

	/**
	 * Signal handler, when the close button is clicked.
	 */
	void closeButtonClicked() {
		this.close();
	}

	/**
	 * Fills the list GUI control by debugging data filename of the selected directory.
	 */
	void fillDebugFilesList() {
		// get the selected directory
		String dirPath = ui.mDirectoryEdit.text();
		File dir = new File(dirPath);

		// get the debugging data files
		DebugDataFilenameFilter filter = new DebugDataFilenameFilter();
		String[] files = dir.list(filter);

		// convert array to list
		List<String> filesList = new ArrayList<String>();
		for (String filename : files) {
			filesList.add(filename);
		}

		ui.mMinefieldsList.clear();
		ui.mMinefieldsList.addItems(filesList);
	}

	/**
	 * Shows the content of the selected file to the minefield view control.
	 * @param item Selected filename from the list of debugging data files.
	 */
	void showDebugData(QListWidgetItem item) {
		String filename = item.text();
		String fullPath = ui.mDirectoryEdit.text() + "/" + filename;

		MinefieldContainer<GUI_FIELD> minefield =
				MinesUtils.<MinefieldContainer<GUI_FIELD>>loadObjectFromFileSafely(fullPath);

		ui.mMinefieldView.createMinefield(minefield);
	}
}
