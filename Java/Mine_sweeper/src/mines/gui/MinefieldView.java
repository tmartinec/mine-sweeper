/**
 * Copyright (C) 2010, LGPL
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * @author Matfyz, fyzmat@gmail.com
 */

package mines.gui;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mines.MinefieldContainer;
import mines.MinesUtils;
import mines.MinefieldContainer.Coord;

import com.trolltech.qt.core.QObject;
import com.trolltech.qt.gui.*;
import mines.GUI_FIELD;

/**
 * Widget, which groups the mine buttons together.
 */
public class MinefieldView extends QGraphicsView{
	/** Map of the contained buttons. */
	Map<Coord, MineButton> mButtonsMap = new HashMap<Coord, MineButton>();

	/** Recipient for the mouse left click signal*/
	QObject mButtonLeftClickRecipient = null;
	/** Name of the handler, when any button is left clicked. */
	String mButtonLeftClickHandler = null;

	/** Recipient for the mouse right click signal*/
	QObject mButtonRightClickRecipient = null;
	/** Name of the handler, when any button is right clicked. */
	String mButtonRightClickHandler = null;

	/** Recipient for the mouse over signal*/
	QObject mButtonMouseMoveRecipient = null;
	/** Name of the handler, when any button is right clicked. */
	String mButtonMouseMoveHandler = null;

	/**
	 * Initializes the minefield view.
	 * @param parent Parent window.
	 */
	public MinefieldView(QWidget parent) {
		super(parent);

		this.setStyleSheet("QGraphicsView { background-color: #F0F0F0; }");
	}

	/**
	 * Sets the recipient and handler for mouse left click.
	 * @param recipient The recipient object.
	 * @param method Name of the handling method.
	 */
	public void connectToButtonsLeftClick(QObject recipient, String method) {
		mButtonLeftClickRecipient = recipient;
		mButtonLeftClickHandler = method;
	}

	/**
	 * Sets the recipient and handler for mouse right click.
	 * @param recipient The recipient object.
	 * @param method Name of the handling method.
	 */
	public void connectToButtonsRightClick(QObject recipient, String method) {
		mButtonRightClickRecipient = recipient;
		mButtonRightClickHandler = method;
	}

	/**
	 * Sets the recipient and handler for mouse over event.
	 * @param recipient The recipient object.
	 * @param method Name of the handling method.
	 */
	public void connectToButtonsMouseOver(QObject recipient, String method) {
		mButtonMouseMoveRecipient = recipient;
		mButtonMouseMoveHandler = method;
	}

	/**
	 * Creates all the mine buttons and a new graphical scene.
	 * @param minefield Minefield to be painted.
	 */
	public void createMinefield(MinefieldContainer<GUI_FIELD> minefield) {
		List<Coord> allFieldsCoords = MinesUtils.getAllFieldsCoords(minefield.getSize());
		
		QGraphicsScene scene = new QGraphicsScene();
		mButtonsMap.clear();
		
		for (Coord coord : allFieldsCoords) {

			GUI_FIELD type = minefield.get(coord);
			MineButton item = new MineButton(coord, type);

			// set the signal recipients and handlers
			if (mButtonLeftClickRecipient != null) {
				item.mLeftButtonPressed.connect(mButtonLeftClickRecipient,  mButtonLeftClickHandler);
			}
			if (mButtonRightClickRecipient != null) {
				item.mRightButtonPressed.connect(mButtonRightClickRecipient,  mButtonRightClickHandler);
			}
			if (mButtonMouseMoveRecipient != null) {
				item.mMouseMovedOver.connect(mButtonMouseMoveRecipient,  mButtonMouseMoveHandler);
			}
			
			mButtonsMap.put(coord, item);
			scene.addItem(item);				
		}
		
		this.setScene(scene);
	}

	/**
	 * Changes the image on the buttons and repaints.
	 * @param minefield Minefield to be painted.
	 */
	public void repaintMinefield(MinefieldContainer<GUI_FIELD> minefield) {
		List<Coord> allFieldsCoords = MinesUtils.getAllFieldsCoords(minefield.getSize());
		
		for (Coord coord : allFieldsCoords) {

			GUI_FIELD type = minefield.get(coord);
			
			MineButton button = mButtonsMap.get(coord);
			button.setButtonType(type);
		}
	}

	/**
	 * Signal handler, when one field should changed its graphical view.
	 * @param coord Coordinates of the field.
	 * @param newValue New value of the field.
	 */
	public void onButtonChanged(Coord coord, GUI_FIELD newValue) {
		MineButton button = mButtonsMap.get(coord);

		button.setButtonType(newValue);
	}
}
