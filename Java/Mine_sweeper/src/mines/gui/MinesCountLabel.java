/**
 * Copyright (C) 2010, LGPL
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * @author Matfyz, fyzmat@gmail.com
 */

package mines.gui;

import com.trolltech.qt.gui.*;

/**
 * Label, which shows the number of remaining mines.
 */
public class MinesCountLabel extends QLabel{

	/**
	 * Initializes the label to zero.
	 * @param parent
	 */
	public MinesCountLabel(QWidget parent) {
		super(parent);
		
		setMinesCount(0);
	}

	/**
	 * Sets the text on the label according to the given count of remaining mines.
	 * @param mines Count of remaining mines.
	 */
	public void setMinesCount(int mines) {
		this.setText("" + mines);
	}
}
