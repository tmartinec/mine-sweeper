/**
 * Copyright (C) 2010, LGPL
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * @author Matfyz, fyzmat@gmail.com
 */

package mines.gui;

import com.trolltech.qt.core.QSize;
import com.trolltech.qt.gui.*;
import mines.gui.generated.Ui_MinesControlPanel;

/**
 * Panel, which contains the control widgets of the main window.
 * This is just because of the placing the reset button to the middle
 * of the window.
 */
public class MinesControlPanel extends QWidget {

	/** GUI for this panel */
    public Ui_MinesControlPanel ui = new Ui_MinesControlPanel();

	/**
	 * Initialize the gui.
	 * @param parent Parent window.
	 */
    public MinesControlPanel(QWidget parent) {
        super(parent);
        ui.setupUi(this);
        
        ui.mMainLayout.removeWidget(ui.mAgainButton);

        int minHeight = Math.max(ui.mAgainButton.sizeHint().height(), this.minimumHeight());
		this.setMinimumHeight(minHeight);
    }

	/**
	 * Places the reset button in the middle of the panel.
	 * @param arg__1 Unknown parameter, which is passed to the super method.
	 */
    protected void resizeEvent(QResizeEvent arg__1) {
		super.resizeEvent(arg__1);

		QSize centeredSize = ui.mAgainButton.sizeHint();
		
		int centre = this.width() / 2;
		int left = centre - (centeredSize.width() / 2);
		
		ui.mAgainButton.setGeometry(left, 0, centeredSize.width(), centeredSize.height());

        int minHeight = Math.max(ui.mAgainButton.height(), this.minimumHeight());
		this.setMinimumHeight(minHeight);
    }
}
