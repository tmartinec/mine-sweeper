/**
 * Copyright (C) 2010, LGPL
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * @author Matfyz, fyzmat@gmail.com
 */

package mines.gui;

import com.trolltech.qt.gui.*;

/**
 * Indeterminable progress bar. Maximum property is set to zero to make the
 * progress bar indeterminable.
 */
public class MineSolverBusyIndicator extends QProgressBar{

	/** Constant for making the progress bar run */
	final int PROGRESS_RUNNING = 0;
	/** Constant for stopping the progress bar */
	final int PROGRESS_STOPPED = 1;

	/**
	 * Initializes the progress bar and stops it.
	 * @param parent
	 */
	public MineSolverBusyIndicator(QWidget parent) {
		super(parent);
		
		this.setMaximum(PROGRESS_STOPPED);
	}

	/**
	 * Signal handler, when the minefield solver has just stopped or runned.
	 * @param solverStopped True, if the solver has stopped.
	 */
	public void onSolverRun(boolean solverStopped) {
		int maximum = solverStopped ? PROGRESS_STOPPED : PROGRESS_RUNNING;
		this.setMaximum(maximum);
	}	
}
