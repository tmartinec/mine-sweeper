/**
 * Copyright (C) 2010, LGPL
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * @author Matfyz, fyzmat@gmail.com
 */

package mines.gui;

import com.trolltech.qt.core.QSize;
import com.trolltech.qt.gui.*;

/**
 * Button for restarting the game.
 */
public class AgainButton extends QPushButton{

	/**
	 * States of the button, which also contains the path of the image.
	 */
	public enum AGAIN_BUTTON_STATE {
		NORMAL("smile.gif"),
		FIRED("fired_smile.gif");
		
		private String mPicturePath = null;
		
		private AGAIN_BUTTON_STATE(String pictureName) {
			mPicturePath = "classpath:resources/pictures/" + pictureName;
		}
		
		public String getPicturePath() {
			return mPicturePath;
		}
	}

	/** Size of the image on the button */
	final static QSize mIconSize = getIconSize();

	/** Current state of the button */
	AGAIN_BUTTON_STATE mState = AGAIN_BUTTON_STATE.NORMAL;

	/**
	 * Gets the size of the images on the button. All images are supposed to have tha
	 * same size.
	 * @return Size of the button images.
	 */
	static QSize getIconSize() {
		QPixmap picture = new QPixmap(AGAIN_BUTTON_STATE.NORMAL.getPicturePath());
		
		return picture.size();
	}

	/**
	 * Initializes the button to the normal state.
	 * @param parent Parent window.
	 */
	public AgainButton(QWidget parent) {
		super(parent);
		
		setState(AGAIN_BUTTON_STATE.NORMAL);
	}

	/**
	 * Sets the state of the button.
	 * @param state State to be set.
	 */
	public void setState(AGAIN_BUTTON_STATE state) {
		mState = state;
		
		String path = state.getPicturePath();
	
		QIcon picture = new QIcon(path);
		if (picture.isNull()) {
			throw new Error("Unable to initialize image: " + path);
		}

		this.setIconSize(mIconSize);
		this.setIcon(picture);
	}

	/**
	 * Signal handler, when the game was restarted
	 */
	void gameRestarted() {
		setState(AGAIN_BUTTON_STATE.NORMAL);
	}

	/**
	 * Signal handler, when the player uncoveres the mined field.
	 */
	void mineFired() {
		setState(AGAIN_BUTTON_STATE.FIRED);	
	}
}
