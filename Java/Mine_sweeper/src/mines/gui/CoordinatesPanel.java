/**
 * Copyright (C) 2010, LGPL
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * @author Matfyz, fyzmat@gmail.com
 */

package mines.gui;

import com.trolltech.qt.gui.QWidget;
import mines.MinefieldContainer.Coord;
import mines.gui.generated.Ui_CoordinatesPanel;

/**
 * Panel, which shows the coordinates on its right side.
 */
public class CoordinatesPanel extends QWidget{

	/** GUI for this panel */
	Ui_CoordinatesPanel ui = new Ui_CoordinatesPanel();

	/**
	 * Initializes the gui.
	 * @param parent Parent window.
	 */
	public CoordinatesPanel(QWidget parent) {
		super(parent);
		ui.setupUi(this);
  	}

	/**
	 * Sets the visibility of this panel. Used as a signal handler.
	 * @param visible True, to visible.
	 */
	public void setVisibility(boolean visible) {
		if (visible) {
			this.show();
		} else {
			this.hide();
		}
	}

	/**
	 * Shows given coordinates. Used as a signal handler.
	 * @param coord Coordinates to show.
	 */
	public void showCoordinates(Coord coord) {
		ui.mCoordinatesLabel.setText("[" + coord.mX + ";" + coord.mY + "]");
	}
}
