/**
 * Copyright (C) 2010, LGPL
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * @author Matfyz, fyzmat@gmail.com
 */

package mines;

import general.IEnumConverter;
import general.IFilterAble;
import general.ReverseEnumMap;
import mines.MineSolver.MinesSolverInternalError;

/**
 * Enumeration for types of fields, which are used by the solver.
 */
public enum SOLVER_FIELD implements IFilterAble, IEnumConverter<Integer>{
	ZERO(0),
	ONE(1),
	TWO(2),
	THREE(3),
	FOUR(4),
	FIVE(5),
	SIX(6),
	SEVEN(7),
	EIGHT(8),
	NINE(9),
	TEN(10),
	ELEVEN(11),
	TWELVE(12),
	COVERED(13),
	MINE(14),
	CLEAR(15);

	/** The integer equivalent for enum value. For numbered fields, this equals
	 the value of the number. */
	private final int mConversionValue;

	/** Mapping from the numerical values of the enum to enumeration constants */
	static final ReverseEnumMap<Integer, SOLVER_FIELD> mReverseMap =
		new ReverseEnumMap<Integer, SOLVER_FIELD>(SOLVER_FIELD.class);

	/**
	 * Constructor for value of SOLVER FIELD enum.
	 * @param conversionValue Numeric representation of enum value. Should be unique for
	 * every value of the enum.
	 */
	SOLVER_FIELD(int conversionValue) {
		mConversionValue = conversionValue;
	}

	/**
	 * Gives the numeric value of the enumeration constant.
	 * @return Value of this enum constant.
	 */
	@Override
	public Integer convert() {
		return this.mConversionValue;
	}

	/**
	 * Used for getting the enumeration constant for increased or decreased
	 * value of given enumaration constant. Make sence only for numbered
	 * constants. Throws an error if the result value is out of number bounds.
	 * @param number Any enumaration constant, which matches the {@link #NUMBER_FLAGS}
	 * flags.
	 * @param addValue Positive or negative value of increment/decrement.
	 * @return The increased or decreased solver field enumeration constant.
	 */
	public static SOLVER_FIELD addToNumber(SOLVER_FIELD number, int addValue) {
		if (!number.isFiltered(NUMBER_FLAGS)) {
			throw new MinesSolverInternalError("Increasing not numbered field " + number);
		}

		int addConversion = number.convert() + addValue;
		if((addConversion < 0) || (addConversion > TWELVE.convert())) {
			throw new MinesSolverInternalError("Addition of " + number + " by " + addValue +
					" is out of bounds.");
		}

		return mReverseMap.get(addConversion);
	}

	static final long COVERED_FLAG =			0x0001;
	static final long ZERO_NUMBER_FLAG =		0x0002;
	static final long NON_ZERO_NUMBER_FLAG =	0x0004;
	static final long MINE_FLAG =				0x0008;
	static final long CLEAR_FLAG =				0x0010;

	static final long NUMBER_FLAGS = ZERO_NUMBER_FLAG | NON_ZERO_NUMBER_FLAG;
	static final long ALL_FLAGS = NUMBER_FLAGS | COVERED_FLAG | MINE_FLAG | CLEAR_FLAG;

	/** This can have even mine flag, because the solver can mark it and be aborted
	 after that, so the field would be revealed yet.*/
	static final long NOT_REVEALED_FLAGS = COVERED_FLAG | CLEAR_FLAG | MINE_FLAG;

	/**
	 * Tries to match this enum value with given bitmask.
	 * @param filter Bitmask used for matching.
	 * @return True, if the current enumeration constant is matched.
	 */
	@Override
	public boolean isFiltered(long filter) {

		switch(this) {
			case ZERO:
				return ((filter & ZERO_NUMBER_FLAG) != 0);
			case ONE:
			case TWO:
			case THREE:
			case FOUR:
			case FIVE:
			case SIX:
			case SEVEN:
			case EIGHT:
			case NINE:
			case TEN:
			case ELEVEN:
			case TWELVE:
				return ((filter & NON_ZERO_NUMBER_FLAG) != 0);

			case COVERED:
				return ((filter & COVERED_FLAG) != 0);

			case MINE:
				return ((filter & MINE_FLAG) != 0);

			case CLEAR:
				return ((filter & CLEAR_FLAG) != 0);

			default:
				throw new MinesSolverInternalError("Unhandled case " + this);
		}
	}
}
