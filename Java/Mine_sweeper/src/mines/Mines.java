/**
 * Copyright (C) 2010, LGPL
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * @author Matfyz, fyzmat@gmail.com
 */

package mines;

import com.trolltech.qt.gui.*;
import mines.gui.generated.Ui_Mines;

/**
 * Main class of the application.
 */
public class Mines extends QMainWindow {

	/** GUI for the main window. */
	public Ui_Mines ui = null;

	/** Main window constructor, initializes the GUI. */
	public Mines() {
		ui = new Ui_Mines();
		ui.setupUi(this);
	}

	/**
	 * The main method. It creates GUI, connects it with the logic part and runs
	 * the GUI application.
	 * @param args Command line arguments - ignored.
	 */
    public static void main(String[] args) {
        QApplication.initialize(args);

		// create the GUI, only one instance of each window is created
        Mines mainClassInstance = new Mines();
		SettingsDialog settingsDialog = new SettingsDialog(mainClassInstance);
		MinesEditor editor = new MinesEditor(mainClassInstance);
		AboutDialog about = new AboutDialog(null);

		// connect the GUI with logic
        MineSweeperLogic logic = MineSweeperLogic.getTheMineSweeperLogic();

        logic.connectWithGUI(mainClassInstance, settingsDialog, editor, about);
        logic.restart();

		// run the application
        mainClassInstance.show();
        QApplication.exec();
    }
}
