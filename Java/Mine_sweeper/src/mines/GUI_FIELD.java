/**
 * Copyright (C) 2010, LGPL
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * @author Matfyz, fyzmat@gmail.com
 */

package mines;

import general.IEnumConverter;
import general.IFilterAble;
import general.ReverseEnumMap;

/**
 * Enumeration for types of fields, which can be seen by the user.
 */
public enum GUI_FIELD implements IFilterAble, IEnumConverter<Integer>{
	ZERO(0),
	ONE(1),
	TWO(2),
	THREE(3),
	FOUR(4),
	FIVE(5),
	SIX(6),
	SEVEN(7),
	EIGHT(8),
	NINE(9),
	TEN(10),
	ELEVEN(11),
	TWELVE(12),
	COVERED(13),
	/** Field is flagged by the user */
	FLAG(14),
	MINE(15),
	/** Mine, which has been uncovered */
	FIRED_MINE(16),
	/** Marked by the solver as mined field */
	MARKED_MINE(17), 
	/** Marked by the solver as numbered field */
	MARKED_CLEAR(18);

	/** The integer equivalent for enum value. For numbered fields, this equals
	 the value of the number. */
	private final int mConversion;

	/**
	 * Constructor for value of GUI FIELD enum.
	 * @param conversion Numeric representation of enum value. Should be unique for
	 * every value of the enum.
	 */
	GUI_FIELD(int conversion) {
		mConversion = conversion;
	}

	/**
	 * Gives the numeric value of the enumeration constant.
	 * @return Value of this enum constant.
	 */
	@Override
	public Integer convert() {
		return mConversion;
	}

	static final long ZERO_NUMBER_FLAG =	0x0001;
	static final long NONZERO_NUMBER_FLAG = 0x0002;
	static final long FLAG_FLAG =			0x0004;
	static final long MINE_FLAG =			0x0008;
	static final long FIRED_MINE_FLAG =		0x0010;
	static final long MARKED_MINE_FLAG =	0x0020;
	static final long MARKED_CLEAR_FLAG =	0x0040;
	static final long COVERED_FLAG =		0x0080;

	static final long NUMBER_FLAGS = ZERO_NUMBER_FLAG | NONZERO_NUMBER_FLAG;
	static final long ALL_FLAGS = NUMBER_FLAGS | FLAG_FLAG | MINE_FLAG |
			FIRED_MINE_FLAG | MARKED_MINE_FLAG | MARKED_CLEAR_FLAG | COVERED_FLAG;
	static final long COVERED_AND_MARKED_CLEAR_FLAGS = COVERED_FLAG | MARKED_CLEAR_FLAG;

	/**
	 * Tries to match this enum value with given bitmask.
	 * @param filter Bitmask used for matching.
	 * @return True, if the current enumeration constant is matched.
	 */
	@Override
	public boolean isFiltered(long filter) {

		switch(this) {
			case ZERO:
				return ((filter & ZERO_NUMBER_FLAG) != 0);
			case ONE:
			case TWO:
			case THREE:
			case FOUR:
			case FIVE:
			case SIX:
			case SEVEN:
			case EIGHT:
			case NINE:
			case TEN:
			case ELEVEN:
			case TWELVE:
				return ((filter & NONZERO_NUMBER_FLAG) != 0);
			case COVERED:
				return ((filter & COVERED_FLAG) != 0);
			case FLAG:
				return ((filter & FLAG_FLAG) != 0);
			case MINE:
				return ((filter & MINE_FLAG) != 0);
			case FIRED_MINE:
				return ((filter & FIRED_MINE_FLAG) != 0);
			case MARKED_MINE:
				return ((filter & MARKED_MINE_FLAG) != 0);
			case MARKED_CLEAR:
				return ((filter & MARKED_CLEAR_FLAG) != 0);

			default:
				throw new Error("Unexpected value in switch " + this);
		}
	}

	/** Mapping from the numerical values of the enum to enumeration constants */
	static final ReverseEnumMap<Integer, GUI_FIELD> mButtonTypesMap =
			new ReverseEnumMap<Integer, GUI_FIELD>(GUI_FIELD.class);

	/**
	 * Returns appropriate GUI FIELD enumeration constant for given number
	 * of mines, which are layed around. Throws an error for invalid counts of mines.
	 * @param mines Count of mines, which are layed next to the field.
	 * @return Numbered GUI field.
	 */
	public static GUI_FIELD getButtonTypeFromMinesCount(int mines) {
		GUI_FIELD result = mButtonTypesMap.get(mines);

		if (!result.isFiltered(GUI_FIELD.NUMBER_FLAGS)) {
			throw new Error("Mines count bigger than expected " + mines);
		}

		return result;
	}
}
