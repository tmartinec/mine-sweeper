/**
 * Copyright (C) 2010, LGPL
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * @author Matfyz, fyzmat@gmail.com
 */

package general;

import java.util.*;

/**
 * Class used for mapping from T values to enum V values. The keys are taken
 * from convert method of {@link general.IEnumConverter IEnumConverter} interface.
 * Used from this <a href=http://www.javaspecialists.eu/archive/Issue113.html> link
 * </a>.
 *
 * @param <T> Type of mapping keys.
 * @param <V> Type of mapped enum values.
 */
public class ReverseEnumMap<T, V extends Enum<V> & IEnumConverter<T>> {
	/** The map itself */
	private Map<T, V> map = new HashMap<T, V>();

	/**
	 * Creates the mapping.
	 * @param valueType Class of enum, which is going to be mapped.
	 */
	public ReverseEnumMap(Class<V> valueType) {
		for (V v : valueType.getEnumConstants()) {
			map.put(v.convert(), v);
		}
	}

	/**
	 * Gets the enum value appropriate for key T.
	 * @param num Key.
	 * @return Enum value for given key.
	 */
	public V get(T num) {
		return map.get(num);
	}
}
