#pragma once

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;


namespace Mines_GUI {

	/// <summary>
	/// Summary for SetupDialog
	///
	/// WARNING: If you change the name of this class, you will need to change the
	///          'Resource File Name' property for the managed resource compiler tool
	///          associated with all .resx files this class depends on.  Otherwise,
	///          the designers will not be able to interact properly with localized
	///          resources associated with this form.
	/// </summary>
	public ref class SetupDialog : public System::Windows::Forms::Form
	{
	public:
		SetupDialog(Int32 nSizeX, Int32 nSizeY, Int32 nMines, Int32 nDevilTurns, String^ prologPath)
		{
			InitializeComponent();

			this->textBoxSizeX->Text = nSizeX.ToString();
			this->textBoxSizeY->Text = nSizeY.ToString();
			this->textBoxMines->Text = nMines.ToString();
			this->textBoxDevilTurns->Text = nDevilTurns.ToString();
			this->textBoxPrologPath->Text = prologPath;

			//na vsech kompech to je podle designera, jenom na tom, co jsem to prezentoval
			//je kus toho dialogu orizlej a me nebavi takovy hovadiny hledat, takze natvrdo
			this->Height = 175;
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~SetupDialog()
		{
			if (components)
			{
				delete components;
			}
		}

	public:
	property Int32 SizeX
	{
		Int32 get() {return Convert::ToInt32(this->textBoxSizeX->Text);}

    };

	property Int32 SizeY
	{
		Int32 get() {return Convert::ToInt32(this->textBoxSizeY->Text);}
    };

	property Int32 Mines
	{
		Int32 get() {return Convert::ToInt32(this->textBoxMines->Text);}
    };

	property Int32 DevilTurns
	{
		Int32 get() {return Convert::ToInt32(this->textBoxDevilTurns->Text);}

    };

	property String^ PrologPath
	{
		String^ get() {return this->textBoxPrologPath->Text;}
	}

	private: System::Windows::Forms::TextBox^  textBoxSizeX;
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::TextBox^  textBoxSizeY;
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::TextBox^  textBoxMines;
	private: System::Windows::Forms::Button^  buttonOK;
	private: System::Windows::Forms::Button^  buttonCancel;
	private: System::Windows::Forms::Label^  label4;
	private: System::Windows::Forms::TextBox^  textBoxDevilTurns;
	private: System::Windows::Forms::TextBox^  textBoxPrologPath;


	private: System::Windows::Forms::Label^  label5;



	protected: 

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->textBoxSizeX = (gcnew System::Windows::Forms::TextBox());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->textBoxSizeY = (gcnew System::Windows::Forms::TextBox());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->textBoxMines = (gcnew System::Windows::Forms::TextBox());
			this->buttonOK = (gcnew System::Windows::Forms::Button());
			this->buttonCancel = (gcnew System::Windows::Forms::Button());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->textBoxDevilTurns = (gcnew System::Windows::Forms::TextBox());
			this->textBoxPrologPath = (gcnew System::Windows::Forms::TextBox());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->SuspendLayout();
			// 
			// textBoxSizeX
			// 
			this->textBoxSizeX->Location = System::Drawing::Point(78, 9);
			this->textBoxSizeX->Name = L"textBoxSizeX";
			this->textBoxSizeX->Size = System::Drawing::Size(100, 20);
			this->textBoxSizeX->TabIndex = 0;
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(34, 12);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(38, 13);
			this->label1->TabIndex = 1;
			this->label1->Text = L"X size:";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(34, 38);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(38, 13);
			this->label2->TabIndex = 3;
			this->label2->Text = L"Y size:";
			// 
			// textBoxSizeY
			// 
			this->textBoxSizeY->Location = System::Drawing::Point(78, 35);
			this->textBoxSizeY->Name = L"textBoxSizeY";
			this->textBoxSizeY->Size = System::Drawing::Size(100, 20);
			this->textBoxSizeY->TabIndex = 2;
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(34, 64);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(38, 13);
			this->label3->TabIndex = 5;
			this->label3->Text = L"Mines:";
			// 
			// textBoxMines
			// 
			this->textBoxMines->Location = System::Drawing::Point(78, 61);
			this->textBoxMines->Name = L"textBoxMines";
			this->textBoxMines->Size = System::Drawing::Size(100, 20);
			this->textBoxMines->TabIndex = 4;
			// 
			// buttonOK
			// 
			this->buttonOK->DialogResult = System::Windows::Forms::DialogResult::OK;
			this->buttonOK->Location = System::Drawing::Point(313, 12);
			this->buttonOK->Name = L"buttonOK";
			this->buttonOK->Size = System::Drawing::Size(85, 20);
			this->buttonOK->TabIndex = 6;
			this->buttonOK->Text = L"OK";
			// 
			// buttonCancel
			// 
			this->buttonCancel->DialogResult = System::Windows::Forms::DialogResult::Cancel;
			this->buttonCancel->Location = System::Drawing::Point(313, 60);
			this->buttonCancel->Name = L"buttonCancel";
			this->buttonCancel->Size = System::Drawing::Size(85, 20);
			this->buttonCancel->TabIndex = 7;
			this->buttonCancel->Text = L"Cancel";
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Location = System::Drawing::Point(12, 89);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(60, 13);
			this->label4->TabIndex = 9;
			this->label4->Text = L"Devil turns:";
			// 
			// textBoxDevilTurns
			// 
			this->textBoxDevilTurns->Location = System::Drawing::Point(78, 86);
			this->textBoxDevilTurns->Name = L"textBoxDevilTurns";
			this->textBoxDevilTurns->Size = System::Drawing::Size(100, 20);
			this->textBoxDevilTurns->TabIndex = 8;
			// 
			// textBoxPrologPath
			// 
			this->textBoxPrologPath->Location = System::Drawing::Point(111, 119);
			this->textBoxPrologPath->Name = L"textBoxPrologPath";
			this->textBoxPrologPath->Size = System::Drawing::Size(287, 20);
			this->textBoxPrologPath->TabIndex = 10;
			this->textBoxPrologPath->Leave += gcnew System::EventHandler(this, &SetupDialog::textBoxPrologPath_Leave);
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->Location = System::Drawing::Point(12, 122);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(93, 13);
			this->label5->TabIndex = 11;
			this->label5->Text = L"Path to plcon.exe:";
			// 
			// SetupDialog
			// 
			this->AcceptButton = this->buttonOK;
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->CancelButton = this->buttonCancel;
			this->ClientSize = System::Drawing::Size(409, 174);
			this->ControlBox = false;
			this->Controls->Add(this->label5);
			this->Controls->Add(this->textBoxPrologPath);
			this->Controls->Add(this->label4);
			this->Controls->Add(this->textBoxDevilTurns);
			this->Controls->Add(this->buttonCancel);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->textBoxMines);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->textBoxSizeY);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->textBoxSizeX);
			this->Controls->Add(this->buttonOK);
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedDialog;
			this->MaximizeBox = false;
			this->MinimizeBox = false;
			this->Name = L"SetupDialog";
			this->Text = L"Setup";
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void textBoxPrologPath_Leave(System::Object^  sender, System::EventArgs^  e) {
				 if (!System::IO::File::Exists(this->textBoxPrologPath->Text))
				 {
					 MessageBox::Show("Warning: Specified path does not exists.");
				 }
			 }
};
}
