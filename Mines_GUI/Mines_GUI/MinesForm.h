/*
 * File:		MinesForm.h
 * Date:		24. b�ezna 2008
 * Author:		Tom� Martinec
 * Projekt:		Z�po�tov� program z p�edm�tu Programov�n� II
 * Popis:		T��da tvo��c� grafick� minov�ho pole.
 */

#pragma once

#include "../../MinesDao/CMinefield.h"
#include "../../MinesDao/PrologMinefield.h"
#include "SetupDialog.h"

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;
using namespace System::Resources;
using namespace System::IO;
using namespace System::Text;

namespace Mines_GUI 
{

private enum FIELD_IND 
{
	NUM_0 = 0,
	NUM_1,
	NUM_2,
	NUM_3,
	NUM_4,
	NUM_5,
	NUM_6,
	NUM_7,
	NUM_8,
	MINE = 9,
	INVISIBLE = 10,
	CLICKED,
	FLAG,
	FIRED,
	FIELD_LAST_INDEX
};

// n�zvy resource pro bitmapy pol��ek
static private char* RESOURCE_FIELD_NAMES[] =
{
	"BMP_FIELD_NUM_0",
	"BMP_FIELD_NUM_1",
	"BMP_FIELD_NUM_2",
	"BMP_FIELD_NUM_3",
	"BMP_FIELD_NUM_4",
	"BMP_FIELD_NUM_5",
	"BMP_FIELD_NUM_6",
	"BMP_FIELD_NUM_7",
	"BMP_FIELD_NUM_8",
	"BMP_FIELD_MINE",
	"BMP_FIELD_INVISIBLE",
	"BMP_FIELD_CLICKED",
	"BMP_FIELD_FLAG",
	"BMP_FIELD_FIRED"
};

private enum AGAINBUTTON_IND
{
	AGBUTT_UP = 0,
	AGBUTT_DOWN,
	AGBUTT_REVEAL,
	AGBUTT_FIRED,
	AGBUTT_DEVIL,
	AGBUTT_LAST_INDEX
};

static private char* RESOURCE_AGAINBUTTON_NAMES[] =
{
	"BMP_AGAINBUTTON_UP",
	"BMP_AGAINBUTTON_DOWN",
	"BMP_AGAINBUTTON_REVEAL",
	"BMP_AGAINBUTTON_FIRED",
	"BMP_AGAINBUTTON_DEVIL",
};

static char* FN_BITMAPS_RES = "Mines_GUI.Bitmaps";
static char* RES_NAME_BMP_SIZE = "FIELDBITMAP_SIZE";

const int N_DEFAULT_X_FIELDS	= 10;
const int N_DEFAULT_Y_FIELDS	= 10;
const int N_DEFAULT_MINES		= N_DEFAULT_X_FIELDS * N_DEFAULT_Y_FIELDS / 5;
const int N_TURNS_LEFT			= 3;

	/// <summary>
	/// Summary for MinesForm
	///
	/// WARNING: If you change the name of this class, you will need to change the
	///          'Resource File Name' property for the managed resource compiler tool
	///          associated with all .resx files this class depends on.  Otherwise,
	///          the designers will not be able to interact properly with localized
	///          resources associated with this form.
	/// </summary>
	public ref class MinesForm : public System::Windows::Forms::Form
	{
	public:
		MinesForm()
		{
			InitializeComponent();

			// otev�en� zdroj�
			System::Reflection::Assembly^ ass =
					System::Reflection::Assembly::GetAssembly(this->GetType());

			String^ strBmpFilename = gcnew String(FN_BITMAPS_RES);
			ResourceManager^ rm = gcnew ResourceManager(strBmpFilename, ass);

			// na�ten� bitmap pro jednotliv� pole ze zdroj�
			Object^ BmpSize = rm->GetObject(gcnew String(RES_NAME_BMP_SIZE));
			m_nFieldSize = System::Convert::ToInt32(BmpSize);

			m_FieldBitmapArray = gcnew array<Bitmap^>(FIELD_LAST_INDEX);
			for(int i = 0; i < FIELD_LAST_INDEX; i++)
			{
				String^ strResource = gcnew String(RESOURCE_FIELD_NAMES[i]);
				m_FieldBitmapArray[i] = (Bitmap^) rm->GetObject(strResource);	
			}

			// bitmapa pro obli�ejov� tla��tko
			m_AgainButtonBitmapArray = gcnew array<Bitmap^>(AGBUTT_LAST_INDEX);
			for(int i = 0; i < AGBUTT_LAST_INDEX; i++)
			{
				String^ strResource = gcnew String(RESOURCE_AGAINBUTTON_NAMES[i]);
				m_AgainButtonBitmapArray[i] = (Bitmap^) rm->GetObject(strResource);	
			}

			this->worker = gcnew BackgroundWorker();
			this->worker->WorkerReportsProgress = true;
			this->worker->WorkerSupportsCancellation = true;
			this->worker->DoWork += gcnew DoWorkEventHandler( this, &MinesForm::worker_DoWork );
			this->worker->RunWorkerCompleted += gcnew RunWorkerCompletedEventHandler( this, &MinesForm::worker_RunCompleted );
			this->worker->ProgressChanged += gcnew ProgressChangedEventHandler( this, &MinesForm::worker_ProgressChanged );

			m_bDoTest = false;
			m_nDevilTurns = N_TURNS_LEFT;
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~MinesForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::ComponentModel::IContainer^  components;
	protected: 

		/// <summary>
		/// Required designer variable.
		/// </summary>

		int m_nXFields;
		int m_nYFields;
		int m_nMines;
		int m_nFieldSize;

		array <Bitmap^>^ m_FieldBitmapArray;			//bitmapy pol��ek
		array <Bitmap^>^ m_AgainButtonBitmapArray;		//bitmapy tla��tek

		array <int, 2>^ m_pView;			
		array <int, 2>^ m_pResultFields;
		bool m_bFired;
		bool m_bDevilFound;
		bool m_bDoTest;

		int m_nDevilTurns;
		int m_nTurnsLeft;

		CMinefield* MinefieldSolver;

		String^ mPrologPath;

		private: System::Windows::Forms::MenuStrip^  menuStrip1;
		private: System::Windows::Forms::OpenFileDialog^  openFileDialog;
		private: System::Windows::Forms::Label^  CoordLabel;
		private: System::Windows::Forms::ToolStripMenuItem^  menuToolStripMenuItem;

		private: System::Windows::Forms::ToolStripMenuItem^  solveMainToolStripMenuItem;

		private: System::Windows::Forms::ToolStripMenuItem^  exitToolStripMenuItem;
		private: System::Diagnostics::Process^  process1;
		private: System::ComponentModel::BackgroundWorker^ worker;



		private: System::Windows::Forms::PictureBox^  MinesBox;
		private: System::Windows::Forms::ToolStripMenuItem^  solveMaxToolStripMenuItem;

		private: System::Windows::Forms::ToolStripMenuItem^  solveToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  devilModeToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  setupToolStripMenuItem;
	private: System::Windows::Forms::Label^  labelTurnsLeft;
	private: System::Windows::Forms::SaveFileDialog^  saveFileDialog;
private: System::Windows::Forms::ToolStripMenuItem^  saveToFileToolStripMenuItem;
private: System::Windows::Forms::ToolStripMenuItem^  loadFromFileToolStripMenuItem;
private: System::Windows::Forms::ToolStripMenuItem^  usePrologToolStripMenuItem;
private: System::Windows::Forms::ToolStripMenuItem^  debugToolStripMenuItem;
private: System::Windows::Forms::ToolStripMenuItem^  runTestingToolStripMenuItem;




	private: System::Windows::Forms::Button^  buttonAgain;

	private: CMinefield* CreateMinefieldSolver()
			 {
				 if (usePrologToolStripMenuItem->Checked)
				 {
					CPrologMinefield* solver = new CPrologMinefield();
					solver->Init(m_nXFields, m_nYFields, FV_HIDDEN, mPrologPath);

					return solver;
				 }
				 else
				 {
					CCMinefield* solver = new CCMinefield();
					solver->Init(m_nXFields, m_nYFields, FV_HIDDEN);

					return solver;
				 }
			 }
	private:
		////////////////////////////////////////////////////////////////////////////////////
		// slou�� k vykreslen� hrac� plochy p�i u�ivatelsk� manipulaci s pol��ky
		void RepaintMinesBox()
		{
			Rectangle rect;
			rect.Height = MinesBox->Height;
			rect.Width = MinesBox->Width;
			Graphics^ g = MinesBox->CreateGraphics();
			PaintEventArgs^ pArgs = gcnew PaintEventArgs(g,rect);
			this->InvokePaint(MinesBox, pArgs);
		};

		////////////////////////////////////////////////////////////////////////////////////
		bool RevealField(int nX, int nY)
		{
			if(m_pView[nX, nY] != INVISIBLE)
				return false;

			int FieldVal = m_pResultFields[nX, nY];

			if(FieldVal == MINE)
				return FireMine(nX, nY);

			MinefieldSolver->RevealField(nX, nY, FieldVal);

			m_pView[nX, nY] = FieldVal;
			if(FieldVal == NUM_0)
			{
				// todle je nulov� pole a odhal�me v�echny pole okolo
				int nAroundX, nAroundY;
				for(int k = 1; k < MAX_AROUNDFIELDS[INDEX_DISTANCE_1]; k++)
				{
					nAroundX = nX + AROUND_FIELDS_INDEXES[k].nX;
					nAroundY = nY + AROUND_FIELDS_INDEXES[k].nY;

					if( (nAroundX == m_nXFields) || (nAroundY == m_nYFields) ||
							(nAroundX == -1) || (nAroundY == -1))
						continue;

					RevealField(nAroundX, nAroundY);
				}
			}

			return true;			
		};

		////////////////////////////////////////////////////////////////////////////////////
		bool FireMine(int nX, int nY)
		{
			for(int i = 0; i < m_nXFields; i++)
				for(int j = 0; j < m_nYFields; j++)
				{
					m_pView[i,j] = m_pResultFields[i,j];	
				}

			m_pView[nX,nY] = FIRED;	
			m_bFired = true;

			return true;
		}

		////////////////////////////////////////////////////////////////////////////////////
		bool MarkFlag(int nX, int nY, bool bMarkOnly)
		{
			//kdy� je pole skryt�, ozna��me ho
			if(m_pView[nX, nY] == INVISIBLE )
				m_pView[nX, nY] = FLAG;
			else
				// kdy� je ozna�en� a m��em ho odzna�it, tak ho odzna��me
				if( (m_pView[nX, nY] == FLAG) && !bMarkOnly)
					m_pView[nX, nY] = INVISIBLE;
				else
					return false;

			return true;
		}

/*		////////////////////////////////////////////////////////////////////////////////////
		CCMinefield* LoadMinefieldFromFile(String^ strFilename)
		{
			// Create two different encodings.
			Encoding^  ascii = Encoding::ASCII;
			Encoding^ unicode = Encoding::Unicode;

			// Convert the string into a Byte->Item[].
			array<Byte>^ unicodeBytes = unicode->GetBytes(strFilename);

			// Perform the conversion from one encoding to the other.
			array<Byte>^ asciiBytes = Encoding::Convert(unicode, ascii, unicodeBytes);

			int nFilename = ascii->GetCharCount(asciiBytes, 0, asciiBytes -> Length);
			char* szFilename = (char*) malloc(nFilename + 1);

			int i;
			for(i = 0; i < strFilename->Length; i++)
				szFilename[i] = asciiBytes[i];
			szFilename[i] = 0;
			
			CCMinefield* pMinefield = new CCMinefield(szFilename);
			if(pMinefield->IsEmpty())
			{
				delete pMinefield;
				free(szFilename);
				return NULL;
o			}
			free(szFilename);

			return pMinefield;
		}
*/
		////////////////////////////////////////////////////////////////////////////////////
		bool Solve(bool bSolveMax)
		{
			MinesBox->Enabled = false;
			solveToolStripMenuItem->Enabled = false;
			solveMaxToolStripMenuItem->Enabled = false;
			buttonAgain->Enabled = false;

			try
			{
				worker->RunWorkerAsync(bSolveMax);
			}
			catch(System::Reflection::TargetInvocationException^ e)
			{
				System::Windows::Forms::MessageBox::Show(e->ToString());
			}
			
			return true;
		}

		////////////////////////////////////////////////////////////////////////////////////
		bool LoadFields(int nSizeX, int nSizeY, int nMines)
		{
			if(nMines > nSizeX * nSizeY)
				nMines = nSizeX * nSizeY;

			m_nXFields = nSizeX;
			m_nYFields = nSizeY;

			m_nTurnsLeft = m_nDevilTurns;
			labelTurnsLeft->Text = "Turns left: " + m_nTurnsLeft.ToString();

			m_nMines = nMines;

			CreateRandomMinefield(m_nXFields, m_nYFields, nMines);
//			FileToResultField();
			ResultFieldToFile(m_nXFields, m_nYFields);

			m_bFired = false;
			m_bDevilFound = false;

			if(MinefieldSolver)
			{
				MinefieldSolver->Close();
				delete MinefieldSolver;
			}

			MinefieldSolver = CreateMinefieldSolver();

			MinesBox->Size = System::Drawing::Size(m_nXFields * m_nFieldSize, (m_nYFields * m_nFieldSize));
			m_pView = gcnew array<int, 2>(m_nXFields, m_nYFields);

			FillFields(m_pView,INVISIBLE);
			this->Width = MinesBox->Width;
			buttonAgain->Image = m_AgainButtonBitmapArray[AGBUTT_UP];

			return true;
		}

		////////////////////////////////////////////////////////////////////////////////////
		bool Again()
		{
			LoadFields(m_nXFields, m_nYFields, m_nMines);
			MinesBox->Invalidate();
			MinesBox->Enabled = true;

			return true;
		}

		////////////////////////////////////////////////////////////////////////////////////
		bool FillFields(array <int, 2>^ pFields,  int nValue)
		{
			for(int i = 0; i < m_nXFields; i++)
				for(int j = 0; j < m_nYFields; j++)
				{
					pFields[i,j] = nValue;
				}

			return true;
		};

		////////////////////////////////////////////////////////////////////////////////////
		bool CreateRandomMinefield(int nXFields, int nYFields, int nMines)
		{
			// napln�n� minami na n�hodn� pozice
			m_pResultFields = gcnew array<int, 2>(nXFields, nYFields);
			FillFields(m_pResultFields, INVISIBLE);

			Random^ RandomGen = gcnew Random();
			int nX, nY;
			for(int i = 0; i < nMines; i++)
			{
				nX = RandomGen->Next(nXFields);
				nY = RandomGen->Next(nYFields);

				while(m_pResultFields[nX,nY] == MINE) 
				{
					nX++;
					if(nX >= m_nXFields)
					{
						nX = 0;
						nY++;
					}
					if(nY >= m_nYFields)
						nY = 0;
				}

				m_pResultFields[nX,nY] = MINE;
			}

			// 
			int nAroundMines;
			int nAroundX, nAroundY;
			for(int i = 0; i < nXFields; i++)
				for(int j = 0; j < nYFields; j++)
				{
					if(m_pResultFields[i,j] == MINE)
						continue;

					// st�ed m� index 0
					nAroundMines = 0;
					for(int k = 1; k < MAX_AROUNDFIELDS[INDEX_DISTANCE_1]; k++)
					{
						nAroundX = i + AROUND_FIELDS_INDEXES[k].nX;
						nAroundY = j + AROUND_FIELDS_INDEXES[k].nY;

						if( (nAroundX == nXFields) || (nAroundY == nYFields) ||
							(nAroundX == -1) || (nAroundY == -1))
							continue;

						if(m_pResultFields[nAroundX, nAroundY] == MINE)
							nAroundMines++;
					}

					m_pResultFields[i,j] = nAroundMines;
				}

			return true;
		};

		//////////////////////////////////////////////////////////////////////////////////////////
		void AddTextToFile( FileStream^ fs, String^ value )
		{
			array<Byte>^info = (gcnew UTF8Encoding( true ))->GetBytes( value );
			fs->Write( info, 0, info->Length );
		}

		//////////////////////////////////////////////////////////////////////////////////////////
		void SaveToFile()
		{
			if(saveFileDialog->ShowDialog() == ::DialogResult::OK)
			{
				String^ filename = saveFileDialog->FileName;
				int index = filename->IndexOf('.');
				String^ viewFilename = index == -1 ? filename + ".vie" : filename->Substring(0, index) + ".vie";
				FileStream^ fs = File::Create(viewFilename);
				try
				{
					AddTextToFile( fs, m_nXFields.ToString() + " " + m_nYFields.ToString() + "\r\n");
					String^ str = gcnew String("");
					for(int i = 0; i < m_nYFields; i++ )
					{
						for(int j = 0; j < m_nXFields; j++ )
						{
							char c[2];
							c[0] = CCMinefield::GetChar(m_pView[j,i]);
							c[1] = 0;
							str = gcnew String(c);
							str += " ";
							AddTextToFile( fs, str );
						}
						AddTextToFile( fs, "\r\n" );
					}
				}
				finally
				{
					if ( fs )
						delete (IDisposable^)fs;
				}	

				String^ resultFilename = index == -1 ? filename + ".res" : filename->Substring(0, index) + ".res";
				fs = File::Create(resultFilename);
				try
				{
					AddTextToFile( fs, m_nXFields.ToString() + " " + m_nYFields.ToString() + "\r\n");
					String^ str = gcnew String("");
					for(int i = 0; i < m_nYFields; i++ )
					{
						for(int j = 0; j < m_nXFields; j++ )
						{
							char c[2];
							c[0] = CCMinefield::GetChar(m_pResultFields[j,i]);
							c[1] = 0;
							str = gcnew String(c);
							str += " ";
							AddTextToFile( fs, str );
						}
						AddTextToFile( fs, "\r\n" );
					}
				}
				finally
				{
					if ( fs )
						delete (IDisposable^)fs;
				}	
			}
		}



		//////////////////////////////////////////////////////////////////////////////////////////
		bool ResultFieldToFile(int nX, int nY)
		{
			FileStream^ fs = File::Create("Last.txt");
			try
			{
				AddTextToFile( fs, nX.ToString() + " " + nY.ToString() + "\r\n" );
				String^ str = gcnew String("");
				for(int i = 0; i < m_nXFields; i++ )
				{
					for(int j = 0; j < m_nYFields; j++ )
					{
						char c[2];
						c[0] = CCMinefield::GetChar(m_pResultFields[i,j]);
						c[1] = 0;
						str = gcnew String(c);
						str += " ";
						AddTextToFile( fs, str );
					}
					AddTextToFile( fs, "\r\n" );
				}
			}
			finally
			{
				if ( fs )
					delete (IDisposable^)fs;
			}	

			return true;
		}

		//////////////////////////////////////////////////////////////////////////////////////////
		void LoadFieldsFromFile()
		{
			if(openFileDialog->ShowDialog() == ::DialogResult::OK)
			{
				int dotIndex = openFileDialog->FileName->LastIndexOf(".");
				String^ withoutExtension = openFileDialog->FileName->Substring(0, dotIndex);
				FileStream^ fs = File::OpenRead(withoutExtension + ".res");
				try
				{
					array<Byte>^b = gcnew array<Byte>((int) fs->Length);
					fs->Read( b, 0, b->Length );
					//UTF8Encoding^ temp = gcnew UTF8Encoding( true );
					array<Char>^ arr = gcnew array<Char>(b->Length);
					for(int i = 0; i < b->Length; i++)
					{
						arr[i] = Convert::ToChar(b[i]);
					}

					String^ line = gcnew String(arr, 0, 40);
					int space = line->IndexOf(" ");
					m_nXFields = Int32::Parse(line->Substring(0, space));

					int cr = line->IndexOf("\r");
					String^ nextNum = line->Substring(space + 1, cr - (space + 1));
					m_nYFields = Int32::Parse(nextNum);

					m_pResultFields = gcnew array<int, 2>(m_nXFields, m_nYFields);

					Char c;
					int nOff = line->IndexOf("\n") + 1;
					for(int i = 0; i < m_nYFields; i++ )
					{
						for(int j = 0; j < m_nXFields; j++ )
						{
							c = arr[nOff];
							nOff += 2;

							m_pResultFields[j,i] = CCMinefield::GetFieldVal((char) c);
						}
						nOff +=2;
					}

				}
				finally
				{
					 if ( fs )
						delete (IDisposable^)fs;
				}

				if(MinefieldSolver)
				{
					MinefieldSolver->Close();
					delete MinefieldSolver;
				}

				MinefieldSolver = CreateMinefieldSolver();

				fs = File::OpenRead(withoutExtension + ".vie");
				try
				{
					array<Byte>^b = gcnew array<Byte>((int) fs->Length);
					fs->Read( b, 0, b->Length );
					//UTF8Encoding^ temp = gcnew UTF8Encoding( true );
					array<Char>^ arr = gcnew array<Char>(b->Length);
					for(int i = 0; i < b->Length; i++)
					{
						arr[i] = Convert::ToChar(b[i]);
					}

					String^ line = gcnew String(arr, 0, 40);
					int space = line->IndexOf(" ");
					m_nXFields = Int32::Parse(line->Substring(0, space));

					int cr = line->IndexOf("\r");
					m_nYFields = Int32::Parse(line->Substring(space + 1, cr - (space + 1)));

					m_pView = gcnew array<int, 2>(m_nXFields, m_nYFields);

					Char c;
					int nOff = 7;
					for(int i = 0; i < m_nYFields; i++ )
					{
						for(int j = 0; j < m_nXFields; j++ )
						{
							c = arr[nOff];
							nOff += 2;

							m_pView[j,i] = CCMinefield::GetFieldVal((char) c);
							COORD coord;
							coord.nX = j;
							coord.nY = i;
							MinefieldSolver->RevealField(coord, m_pView[j,i]);
						}
						nOff +=2;
					}
				}
				finally
				{
					 if ( fs )
						delete (IDisposable^)fs;
				}

				m_nTurnsLeft = 0;
				labelTurnsLeft->Text = "Turns left: " + m_nTurnsLeft.ToString();
				devilModeToolStripMenuItem->Checked = true;
				labelTurnsLeft->Visible = true;

				m_bFired = false;
				m_bDevilFound = false;

				MinesBox->Size = System::Drawing::Size(m_nXFields * m_nFieldSize, (m_nYFields * m_nFieldSize));
				buttonAgain->Image = m_AgainButtonBitmapArray[AGBUTT_UP];

				MinesBox->Invalidate();
				this->Width = MinesBox->Width;
				this->Invalidate();
				MinesBox->Enabled = true;



			}
		}

		//////////////////////////////////////////////////////////////////////////////////////////
		bool FileToResultField()
		{
			FileStream^ fs = File::OpenRead("Last.txt");

			try
			{
				StreamReader^ reader = gcnew StreamReader(fs);
				String^ line = reader->ReadLine();

				array<String^>^ separators = gcnew array<String^>(1);
				separators[0] = " ";

				array<String^>^ strSizes = line->Split(separators, System::StringSplitOptions::RemoveEmptyEntries);
				m_nXFields = Int32::Parse(strSizes[0]);
				m_nYFields = Int32::Parse(strSizes[1]);

				String^ fileContent = reader->ReadToEnd();

				m_pResultFields = gcnew array<int, 2>(m_nXFields, m_nYFields);
	
				int nOff = 0;
				Char c;
				for(int i = 0; i < m_nXFields; i++ )
				{
					for(int j = 0; j < m_nYFields; j++ )
					{
						
						c = fileContent[nOff];

						m_pResultFields[i,j] = CCMinefield::GetFieldVal((char) c);
						nOff += 2;

					}
					nOff +=2;
				}

			}
			finally
			{
				 if ( fs )
					delete (IDisposable^)fs;
			}

			return true;
		}

//////////////////////////////////////////////////////////////////////////////////////////
		void worker_DoWork( Object^ sender, DoWorkEventArgs^ e )
		 {
			// Get the BackgroundWorker that raised this event.
		    BackgroundWorker^ worker = dynamic_cast<BackgroundWorker^>(sender);

			int nRuns = 0;

			bool bSolveMax = (bool) e->Argument;
			bool bChange;
			do
			{
				bChange = false;
				// dokud je zm�na, tak proj�d�me celou hrac� plochu
				for(int i = 0; i < m_nXFields; i++ )
					for(int j = 0; j < m_nYFields; j++ )
					{
						if (m_pView[i,j] != INVISIBLE)
						{
							continue;
						}

						int nStatus = MinefieldSolver->SolveOneField(i, j);

						switch(nStatus)
						{

						case SOLVE_CANNOT_BE_MINE:
						{
							if(RevealField(i, j))
								bChange = true;

							break;
						}
						case SOLVE_MUST_BE_MINE:
						{
							if(MarkFlag(i, j, true))
								bChange = true;

							break;
						}
						}

						if(m_bFired)
						{
							// nem�lo by nikdy nastat
							ResultFieldToFile(m_nXFields, m_nYFields);
							MessageBox::Show("Do�lo ke �patn�mu vyhodnocen� na sou�adnic�ch "
								+ i.ToString() + ":" + j.ToString()
								+ ": Pr�chod - " + nRuns.ToString() );
							return;
						}

						worker->ReportProgress(((i * m_nYFields + j) * 100) / (m_nYFields * m_nXFields),
							" " + i.ToString() + ":" + j.ToString());

						if(worker->CancellationPending)
						{
							e->Cancel = true;
							return;
						}

					}

				worker->ReportProgress(0);
				nRuns++;

			}
			while(bChange && bSolveMax);
		 }

//////////////////////////////////////////////////////////////////////////////////////////
		void worker_RunCompleted(Object^ sender, RunWorkerCompletedEventArgs^ e )
		{
			if(e->Cancelled)
				return;

			MinesBox->Enabled = true;
			solveToolStripMenuItem->Enabled = true;
			solveMaxToolStripMenuItem->Enabled = true;
			buttonAgain->Enabled = true;

			if(m_bDoTest && !m_bFired)
			{
				LoadFields(m_nXFields, m_nYFields, m_nXFields * m_nYFields / 5);
				this->Invalidate();

				// v p��pad� skon�en� testu p�iprav novou plochu pro testov�n�
				Test();
			}
		}

//////////////////////////////////////////////////////////////////////////////////////////
		void worker_ProgressChanged(System::Object ^ sender,System::ComponentModel::ProgressChangedEventArgs ^ e)
		{
			Threading::Thread::CurrentThread->Priority = Threading::ThreadPriority::AboveNormal;

//			MinesProgress->Value = e->ProgressPercentage;
			MinesBox->Invalidate();

			Threading::Thread::CurrentThread->Priority = Threading::ThreadPriority::Normal;
		}

//////////////////////////////////////////////////////////////////////////////////////////
		bool doDevil(int nX, int nY)
		{
			if(m_nTurnsLeft > 0)
			{
				m_nTurnsLeft--;
				labelTurnsLeft->Text = "Turns left: " + m_nTurnsLeft.ToString();
			}
			else
			{
				COORD coord = {nX, nY};
				FIELDVAL* pSolution = ((CCMinefield*) MinefieldSolver)->FindDevilSolution(coord);
				if(pSolution)
				{
					// na�li jsme devil �e�en� a p�ep�eme j�m skute�n� hrac� pole
					for(int i = 0; i < m_nXFields; i++)
						for(int j = 0; j < m_nYFields; j++)
						{
							FIELDVAL fieldVal = *(pSolution + i * m_nYFields + j);
							if(fieldVal == FV_MINE)
								m_pView[i, j] = MINE;
						}

					MinesBox->Enabled = false;
					buttonAgain->Image = m_AgainButtonBitmapArray[AGBUTT_DEVIL];
					m_nTurnsLeft = m_nDevilTurns;

					free(pSolution);
					return true;
				}
			}

			return false;
		}

//////////////////////////////////////////////////////////////////////////////////////////
		void Test()
		{
			m_bDoTest = true;
			bool bSolveMax = true;

			int i = 0;
			int j = 0;
			while(m_pResultFields[i,j] != NUM_0)
				{
					i++;
					if(i == m_nXFields)
					{
						i = 0;
						j++;
					}
					if(j == m_nYFields)
						j = 0;
				}
			RevealField(i,j);

			Solve(true);
		}

//////////////////////////////////////////////////////////////////////////////////////////
#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->menuStrip1 = (gcnew System::Windows::Forms::MenuStrip());
			this->menuToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->solveMainToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->solveToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->solveMaxToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->devilModeToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->usePrologToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->setupToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->loadFromFileToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->saveToFileToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->exitToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->MinesBox = (gcnew System::Windows::Forms::PictureBox());
			this->openFileDialog = (gcnew System::Windows::Forms::OpenFileDialog());
			this->CoordLabel = (gcnew System::Windows::Forms::Label());
			this->process1 = (gcnew System::Diagnostics::Process());
			this->buttonAgain = (gcnew System::Windows::Forms::Button());
			this->labelTurnsLeft = (gcnew System::Windows::Forms::Label());
			this->saveFileDialog = (gcnew System::Windows::Forms::SaveFileDialog());
			this->debugToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->runTestingToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->menuStrip1->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->MinesBox))->BeginInit();
			this->SuspendLayout();
			// 
			// menuStrip1
			// 
			this->menuStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {this->menuToolStripMenuItem, 
				this->debugToolStripMenuItem});
			this->menuStrip1->Location = System::Drawing::Point(0, 0);
			this->menuStrip1->Name = L"menuStrip1";
			this->menuStrip1->Size = System::Drawing::Size(409, 24);
			this->menuStrip1->TabIndex = 0;
			this->menuStrip1->Text = L"menuStrip1";
			// 
			// menuToolStripMenuItem
			// 
			this->menuToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(7) {this->solveMainToolStripMenuItem, 
				this->devilModeToolStripMenuItem, this->usePrologToolStripMenuItem, this->setupToolStripMenuItem, this->loadFromFileToolStripMenuItem, 
				this->saveToFileToolStripMenuItem, this->exitToolStripMenuItem});
			this->menuToolStripMenuItem->Name = L"menuToolStripMenuItem";
			this->menuToolStripMenuItem->Size = System::Drawing::Size(45, 20);
			this->menuToolStripMenuItem->Text = L"&Menu";
			// 
			// solveMainToolStripMenuItem
			// 
			this->solveMainToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {this->solveToolStripMenuItem, 
				this->solveMaxToolStripMenuItem});
			this->solveMainToolStripMenuItem->Name = L"solveMainToolStripMenuItem";
			this->solveMainToolStripMenuItem->Size = System::Drawing::Size(152, 22);
			this->solveMainToolStripMenuItem->Text = L"&Solve";
			// 
			// solveToolStripMenuItem
			// 
			this->solveToolStripMenuItem->Name = L"solveToolStripMenuItem";
			this->solveToolStripMenuItem->ShortcutKeys = System::Windows::Forms::Keys::F9;
			this->solveToolStripMenuItem->Size = System::Drawing::Size(148, 22);
			this->solveToolStripMenuItem->Text = L"&Solve";
			this->solveToolStripMenuItem->Click += gcnew System::EventHandler(this, &MinesForm::solveToolStripMenuItem1_Click);
			// 
			// solveMaxToolStripMenuItem
			// 
			this->solveMaxToolStripMenuItem->Name = L"solveMaxToolStripMenuItem";
			this->solveMaxToolStripMenuItem->ShortcutKeys = System::Windows::Forms::Keys::F10;
			this->solveMaxToolStripMenuItem->Size = System::Drawing::Size(148, 22);
			this->solveMaxToolStripMenuItem->Text = L"Solve &max";
			this->solveMaxToolStripMenuItem->Click += gcnew System::EventHandler(this, &MinesForm::solveMaxToolStripMenuItem1_Click);
			// 
			// devilModeToolStripMenuItem
			// 
			this->devilModeToolStripMenuItem->Enabled = false;
			this->devilModeToolStripMenuItem->Name = L"devilModeToolStripMenuItem";
			this->devilModeToolStripMenuItem->ShortcutKeys = System::Windows::Forms::Keys::F11;
			this->devilModeToolStripMenuItem->Size = System::Drawing::Size(152, 22);
			this->devilModeToolStripMenuItem->Text = L"&Devil mode";
			this->devilModeToolStripMenuItem->Click += gcnew System::EventHandler(this, &MinesForm::devilModeToolStripMenuItem_Click);
			// 
			// usePrologToolStripMenuItem
			// 
			this->usePrologToolStripMenuItem->Checked = true;
			this->usePrologToolStripMenuItem->CheckState = System::Windows::Forms::CheckState::Checked;
			this->usePrologToolStripMenuItem->Name = L"usePrologToolStripMenuItem";
			this->usePrologToolStripMenuItem->Size = System::Drawing::Size(152, 22);
			this->usePrologToolStripMenuItem->Text = L"&Use Prolog";
			this->usePrologToolStripMenuItem->Click += gcnew System::EventHandler(this, &MinesForm::usePrologToolStripMenuItem_Click);
			// 
			// setupToolStripMenuItem
			// 
			this->setupToolStripMenuItem->Name = L"setupToolStripMenuItem";
			this->setupToolStripMenuItem->Size = System::Drawing::Size(152, 22);
			this->setupToolStripMenuItem->Text = L"Setu&p";
			this->setupToolStripMenuItem->Click += gcnew System::EventHandler(this, &MinesForm::setupToolStripMenuItem_Click);
			// 
			// loadFromFileToolStripMenuItem
			// 
			this->loadFromFileToolStripMenuItem->Name = L"loadFromFileToolStripMenuItem";
			this->loadFromFileToolStripMenuItem->Size = System::Drawing::Size(152, 22);
			this->loadFromFileToolStripMenuItem->Text = L"&Load test data";
			this->loadFromFileToolStripMenuItem->Click += gcnew System::EventHandler(this, &MinesForm::loadFromFileToolStripMenuItem_Click);
			// 
			// saveToFileToolStripMenuItem
			// 
			this->saveToFileToolStripMenuItem->Name = L"saveToFileToolStripMenuItem";
			this->saveToFileToolStripMenuItem->Size = System::Drawing::Size(152, 22);
			this->saveToFileToolStripMenuItem->Text = L"Save &to file";
			this->saveToFileToolStripMenuItem->Click += gcnew System::EventHandler(this, &MinesForm::saveToFileToolStripMenuItem_Click);
			// 
			// exitToolStripMenuItem
			// 
			this->exitToolStripMenuItem->Name = L"exitToolStripMenuItem";
			this->exitToolStripMenuItem->Size = System::Drawing::Size(152, 22);
			this->exitToolStripMenuItem->Text = L"&Exit";
			this->exitToolStripMenuItem->Click += gcnew System::EventHandler(this, &MinesForm::exitToolStripMenuItem_Click);
			// 
			// MinesBox
			// 
			this->MinesBox->BackColor = System::Drawing::SystemColors::Control;
			this->MinesBox->Location = System::Drawing::Point(0, 50);
			this->MinesBox->Margin = System::Windows::Forms::Padding(0);
			this->MinesBox->Name = L"MinesBox";
			this->MinesBox->Padding = System::Windows::Forms::Padding(2);
			this->MinesBox->Size = System::Drawing::Size(160, 16);
			this->MinesBox->TabIndex = 1;
			this->MinesBox->TabStop = false;
			this->MinesBox->MouseLeave += gcnew System::EventHandler(this, &MinesForm::MinesBox_MouseLeave);
			this->MinesBox->MouseMove += gcnew System::Windows::Forms::MouseEventHandler(this, &MinesForm::MinesBox_MouseMove);
			this->MinesBox->Click += gcnew System::EventHandler(this, &MinesForm::MinesBox_Click);
			this->MinesBox->MouseDown += gcnew System::Windows::Forms::MouseEventHandler(this, &MinesForm::MinesBox_MouseDown);
			this->MinesBox->Paint += gcnew System::Windows::Forms::PaintEventHandler(this, &MinesForm::MinesBox_Paint);
			this->MinesBox->MouseUp += gcnew System::Windows::Forms::MouseEventHandler(this, &MinesForm::MinesBox_MouseUp);
			// 
			// openFileDialog
			// 
			this->openFileDialog->Filter = L"Test data (*.vie)|*.vie";
			this->openFileDialog->InitialDirectory = L"..\\..\\Testy";
			this->openFileDialog->Title = L"Open task file";
			// 
			// CoordLabel
			// 
			this->CoordLabel->AutoSize = true;
			this->CoordLabel->BackColor = System::Drawing::SystemColors::ActiveBorder;
			this->CoordLabel->Location = System::Drawing::Point(-3, 24);
			this->CoordLabel->Name = L"CoordLabel";
			this->CoordLabel->Size = System::Drawing::Size(70, 13);
			this->CoordLabel->TabIndex = 3;
			this->CoordLabel->Text = L"CursorCoords";
			// 
			// process1
			// 
			this->process1->StartInfo->Domain = L"";
			this->process1->StartInfo->LoadUserProfile = false;
			this->process1->StartInfo->Password = nullptr;
			this->process1->StartInfo->StandardErrorEncoding = nullptr;
			this->process1->StartInfo->StandardOutputEncoding = nullptr;
			this->process1->StartInfo->UserName = L"";
			this->process1->SynchronizingObject = this;
			// 
			// buttonAgain
			// 
			this->buttonAgain->Anchor = System::Windows::Forms::AnchorStyles::Top;
			this->buttonAgain->BackColor = System::Drawing::SystemColors::AppWorkspace;
			this->buttonAgain->DialogResult = System::Windows::Forms::DialogResult::OK;
			this->buttonAgain->FlatAppearance->BorderSize = 0;
			this->buttonAgain->FlatAppearance->MouseDownBackColor = System::Drawing::Color::Transparent;
			this->buttonAgain->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->buttonAgain->Location = System::Drawing::Point(195, 24);
			this->buttonAgain->Name = L"buttonAgain";
			this->buttonAgain->Size = System::Drawing::Size(26, 26);
			this->buttonAgain->TabIndex = 6;
			this->buttonAgain->UseVisualStyleBackColor = false;
			this->buttonAgain->MouseClick += gcnew System::Windows::Forms::MouseEventHandler(this, &MinesForm::buttonAgain_MouseClick);
			this->buttonAgain->MouseDown += gcnew System::Windows::Forms::MouseEventHandler(this, &MinesForm::buttonAgain_MouseDown);
			this->buttonAgain->MouseUp += gcnew System::Windows::Forms::MouseEventHandler(this, &MinesForm::buttonAgain_MouseUp);
			// 
			// labelTurnsLeft
			// 
			this->labelTurnsLeft->AutoSize = true;
			this->labelTurnsLeft->BackColor = System::Drawing::SystemColors::ActiveBorder;
			this->labelTurnsLeft->Location = System::Drawing::Point(-3, 37);
			this->labelTurnsLeft->Name = L"labelTurnsLeft";
			this->labelTurnsLeft->Size = System::Drawing::Size(0, 13);
			this->labelTurnsLeft->TabIndex = 7;
			this->labelTurnsLeft->Visible = false;
			// 
			// saveFileDialog
			// 
			this->saveFileDialog->Filter = L"Test data (*.vie)|*.vie";
			this->saveFileDialog->InitialDirectory = L"..\\..\\Testy";
			// 
			// debugToolStripMenuItem
			// 
			this->debugToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) {this->runTestingToolStripMenuItem});
			this->debugToolStripMenuItem->Name = L"debugToolStripMenuItem";
			this->debugToolStripMenuItem->Size = System::Drawing::Size(50, 20);
			this->debugToolStripMenuItem->Text = L"&Debug";
			// 
			// runTestingToolStripMenuItem
			// 
			this->runTestingToolStripMenuItem->Name = L"runTestingToolStripMenuItem";
			this->runTestingToolStripMenuItem->Size = System::Drawing::Size(152, 22);
			this->runTestingToolStripMenuItem->Text = L"&Run testing";
			this->runTestingToolStripMenuItem->Click += gcnew System::EventHandler(this, &MinesForm::runTestingToolStripMenuItem_Click);
			// 
			// MinesForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->AutoScroll = true;
			this->AutoSize = true;
			this->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
			this->BackColor = System::Drawing::SystemColors::AppWorkspace;
			this->ClientSize = System::Drawing::Size(409, 241);
			this->Controls->Add(this->labelTurnsLeft);
			this->Controls->Add(this->CoordLabel);
			this->Controls->Add(this->MinesBox);
			this->Controls->Add(this->menuStrip1);
			this->Controls->Add(this->buttonAgain);
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedSingle;
			this->MainMenuStrip = this->menuStrip1;
			this->MinimumSize = System::Drawing::Size(160, 87);
			this->Name = L"MinesForm";
			this->Text = L"MinesForm";
			this->Load += gcnew System::EventHandler(this, &MinesForm::MinesForm_Load);
			this->FormClosed += gcnew System::Windows::Forms::FormClosedEventHandler(this, &MinesForm::MinesForm_FormClosed);
			this->menuStrip1->ResumeLayout(false);
			this->menuStrip1->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->MinesBox))->EndInit();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
///////////////////////////////////////////////////////////////////////////////////////////////////////////
private: System::Void MinesForm_Load(System::Object^  sender, System::EventArgs^  e) 
			 {

				m_nXFields = N_DEFAULT_X_FIELDS;
				m_nYFields = N_DEFAULT_Y_FIELDS;

				m_nTurnsLeft = N_TURNS_LEFT;
				int nMines = N_DEFAULT_MINES;

				mPrologPath = "c:\\Program Files\\pl\\bin\\plcon.exe";
				if (!File::Exists(mPrologPath))
				{
					MessageBox::Show("Please specify correct path for prolog plcon.exe.");
					SetupDialog^ dlg = gcnew SetupDialog(m_nXFields, m_nYFields, nMines, m_nTurnsLeft, mPrologPath);

					dlg->ShowDialog();

					 m_nXFields = dlg->SizeX;
					 m_nYFields = dlg->SizeY;
					 m_nMines = dlg->Mines;
					 m_nDevilTurns = dlg->DevilTurns;
					 mPrologPath = dlg->PrologPath;
				}

				 LoadFields(m_nXFields,
						   m_nYFields,
						   m_nYFields);
			 }

///////////////////////////////////////////////////////////////////////////////////////////////////////////
private: System::Void MinesBox_Paint(System::Object^  sender, System::Windows::Forms::PaintEventArgs^  e) 
			 {
 		         Graphics^ g = e->Graphics;

				 for(int i = 0; i < m_nXFields; i++)
				 {
					for(int j = 0; j < m_nYFields; j++)
					{
						g->DrawImage(m_FieldBitmapArray[m_pView[i,j]],i * m_nFieldSize, j *m_nFieldSize);
					}
				 }
			 }

///////////////////////////////////////////////////////////////////////////////////////////////////////////
private: System::Void MinesBox_Click(System::Object^  sender, System::EventArgs^  e) 
		 {
			MouseEventArgs^ mArgs = (MouseEventArgs^) e;
			
			int nX = mArgs->X / m_nFieldSize;
			int nY = mArgs->Y / m_nFieldSize;

			// p�i lev�m tla��tku odhal�me pole, pokud nen� ozna�en�
			if( (mArgs->Button == ::MouseButtons::Left) && (m_pView[nX, nY] != FLAG))
			{
				if(devilModeToolStripMenuItem->Enabled && devilModeToolStripMenuItem->Checked)
				{
					m_bDevilFound = doDevil(nX, nY);
				}

				if(!m_bDevilFound)
				{
					RevealField(nX, nY);
				}

				RepaintMinesBox();
			}

			// p�i prav�m tla��tku ozna��me/odzna��me vlajku
			if(mArgs->Button == ::MouseButtons::Right)
			{
				MarkFlag(nX, nY, false);
				RepaintMinesBox();
			}

		 }
/*
///////////////////////////////////////////////////////////////////////////////////////////////////////////
private: System::Void loadFromFileToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
		 {
			 openFileDialog->Title = "Open task file";
			 openFileDialog->ShowDialog();

			 if(openFileDialog->FileName->Length == 0)
				 return;

			 String^ task = openFileDialog->FileName;

			 openFileDialog->Title = "Open result file";
			 openFileDialog->ShowDialog();
			 if(openFileDialog->FileName->Length == 0)
				 return;

			 String^ result = openFileDialog->FileName;

			 if( !(MinefieldResult = LoadMinefieldFromFile(result)) )
			 {
				MessageBox::Show("Nepoda�ilo se na��st data ze zadan�ho souboru");
				MinesBox->Enabled = false;
				return;
			 }
			 MinesBox->Enabled = true;

 			 delete MinefieldSolver;

			 int nX, nY;
			 MinefieldResult->GetSize(&nX, &nY);
			 MinefieldSolver = new CCMinefield(nX, nY, FV_HIDDEN);

		 }
*/

/////////////////////////////////////////////////////////////////////////////////////////////////////////
private: System::Void exitToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) 
		 {
			Close();			 
		 }

///////////////////////////////////////////////////////////////////////////////////////////////////////////
private: System::Void MinesForm_FormClosed(System::Object^  sender, System::Windows::Forms::FormClosedEventArgs^  e) 
		 {
			 worker->CancelAsync();
			 while(this->worker->IsBusy)
			 {
				 System::Windows::Forms::Application::DoEvents();
			 }

			 MinefieldSolver->Close();
			 delete MinefieldSolver;
		 }
///////////////////////////////////////////////////////////////////////////////////////////////////////////
private: System::Void MinesBox_MouseMove(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) 
		 {
			MouseEventArgs^ mArgs = (MouseEventArgs^) e;
			
			int nX = mArgs->X / m_nFieldSize;
			int nY = mArgs->Y / m_nFieldSize;

			CoordLabel->Text = "x:" + nX.ToString() + " y:" + nY.ToString();
		 }
///////////////////////////////////////////////////////////////////////////////////////////////////////////
private: System::Void MinesBox_MouseLeave(System::Object^  sender, System::EventArgs^  e) 
		 {
			CoordLabel->Text = "";
		 }
///////////////////////////////////////////////////////////////////////////////////////////////////////////
private: System::Void sameParametrsToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
		 {
			 Again();
		 }
///////////////////////////////////////////////////////////////////////////////////////////////////////////
private: System::Void newParametrsToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) 
		 {
		 }
///////////////////////////////////////////////////////////////////////////////////////////////////////////
private: System::Void buttonAgain_MouseClick(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e)
		 {
			Again();
		 }
///////////////////////////////////////////////////////////////////////////////////////////////////////////
private: System::Void buttonAgain_MouseDown(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) 
		 {
			buttonAgain->Image = m_AgainButtonBitmapArray[AGBUTT_DOWN];
		 }
///////////////////////////////////////////////////////////////////////////////////////////////////////////
private: System::Void buttonAgain_MouseUp(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) 
		 {
			buttonAgain->Image = m_AgainButtonBitmapArray[AGBUTT_UP];
		 }
///////////////////////////////////////////////////////////////////////////////////////////////////////////
private: System::Void MinesBox_MouseDown(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) 
		 {
			buttonAgain->Image = m_AgainButtonBitmapArray[AGBUTT_REVEAL];
		 }
///////////////////////////////////////////////////////////////////////////////////////////////////////////
private: System::Void MinesBox_MouseUp(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) 
		 {
			if(m_bDevilFound)
			{
				buttonAgain->Image = m_AgainButtonBitmapArray[AGBUTT_DEVIL];
				return;
			}

			if(m_bFired)
			{
				buttonAgain->Image = m_AgainButtonBitmapArray[AGBUTT_FIRED];
				return;
			}

			buttonAgain->Image = m_AgainButtonBitmapArray[AGBUTT_UP];
		 }
///////////////////////////////////////////////////////////////////////////////////////////////////////////
private: System::Void solveMaxToolStripMenuItem1_Click(System::Object^  sender, System::EventArgs^  e) 
		 {
			 Solve(true);
		 }
///////////////////////////////////////////////////////////////////////////////////////////////////////////
private: System::Void solveToolStripMenuItem1_Click(System::Object^  sender, System::EventArgs^  e) 
		 {
			 Solve(false);
		 }
///////////////////////////////////////////////////////////////////////////////////////////////////////////
private: System::Void testToolStripMenuItem_Click_1(System::Object^  sender, System::EventArgs^  e) 
		 {
			Test();
		 }
///////////////////////////////////////////////////////////////////////////////////////////////////////////
private: System::Void stopTestToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) 
		 {
			 m_bDoTest = false;
		 }
private: System::Void setupToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) 
		 {
			 SetupDialog^ Dlg = gcnew SetupDialog(m_nXFields, m_nYFields, m_nMines, m_nTurnsLeft, mPrologPath);

			 if(Dlg->ShowDialog() == System::Windows::Forms::DialogResult::OK )
			 {
				 // Do something here to handle data from dialog box.
				 if(Dlg->Mines > Dlg->SizeX * Dlg->SizeY)
				 {
					 MessageBox::Show("Too many mines.");
					 return;
				 }

				 m_nXFields = Dlg->SizeX;
				 m_nYFields = Dlg->SizeY;
				 m_nMines = Dlg->Mines;
				 m_nDevilTurns = Dlg->DevilTurns;
				 mPrologPath = Dlg->PrologPath;

				 Again();

				 this->AutoSize = false;
				 this->AutoSize = true;
			 }
		 }
private: System::Void devilModeToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) 
		 {
			 devilModeToolStripMenuItem->Checked = ! (devilModeToolStripMenuItem->Checked);

			 labelTurnsLeft->Visible = devilModeToolStripMenuItem->Checked && devilModeToolStripMenuItem->Enabled;
		 }
private: System::Void againToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
		 }
private: System::Void saveToFileToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			SaveToFile();
		 }
private: System::Void loadFromFileToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			LoadFieldsFromFile();
		 }
private: System::Void usePrologToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			 usePrologToolStripMenuItem->Checked = !usePrologToolStripMenuItem->Checked;
			 devilModeToolStripMenuItem->Enabled = !usePrologToolStripMenuItem->Checked;

			 labelTurnsLeft->Visible = devilModeToolStripMenuItem->Checked && devilModeToolStripMenuItem->Enabled;

			 Again();
		 }
private: System::Void runTestingToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			 Test();
		 }
};
}
