// Mines_GUI.cpp : main project file.

#include "stdafx.h"
#include "MinesForm.h"

#include <crtdbg.h>

using namespace Mines_GUI;
#ifdef _DEBUG 
	#define _CRTDBG_MAP_ALLOC
#endif //_DEBUG 


[STAThreadAttribute]
int main(array<System::String ^> ^args)
{
	// Enabling Windows XP visual effects before any controls are created
	Application::EnableVisualStyles();
	Application::SetCompatibleTextRenderingDefault(false); 

	// Create the main window and run it
	try
	{
		Application::Run(gcnew MinesForm());
	}
	catch(System::Exception^ e)
	{
		System::Windows::Forms::MessageBox::Show(e->ToString());
	}
	_CrtDumpMemoryLeaks();
	return 0;
}
