#define _CRT_SECURE_NO_DEPRECATE

#include "CMinefield.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

////////////////////////////////////////////////////////////////////
void CCMinefield::Init(int nSizeX, int nSizeY, FIELDVAL FieldsVal)
{
	m_nSizeX = nSizeX;
	m_nSizeY = nSizeY;

	m_pFields = (FIELDVAL*) malloc(nSizeX * nSizeY * sizeof(FIELDVAL));
	m_pDevilSolution = NULL;
	for(int i = 0; i < nSizeX * nSizeY; i++)
		*(m_pFields + i) = FieldsVal;

	m_bDevilSolution = false;
}

////////////////////////////////////////////////////////////////////
void CCMinefield::Init(int nSizeX, int nSizeY, const FIELDVAL* pFields)
{
	m_nSizeX = nSizeX;
	m_nSizeY = nSizeY;

	m_pFields = (FIELDVAL*) malloc(nSizeX * nSizeY * sizeof(FIELDVAL));
	m_pDevilSolution = NULL;
	
	memcpy(m_pFields, pFields, nSizeX * nSizeY);

	m_bDevilSolution = false;
}

////////////////////////////////////////////////////////////////////
void CCMinefield::Init(CCMinefield* Field)
{
	Field->GetSize(&m_nSizeX, &m_nSizeY);	
	m_pFields = (FIELDVAL*) malloc(m_nSizeX * m_nSizeY * sizeof(FIELDVAL));

	m_pDevilSolution = NULL;
	for(int i = 0; i < m_nSizeX; i++)
		for(int j = 0; j < m_nSizeY; j++)
			Field->GetField(i, j, m_pFields + j + (i * m_nSizeY) );

	m_bDevilSolution = false;
}

////////////////////////////////////////////////////////////////////
void CCMinefield::Init(const char* szFilename)
{
	m_nSizeX = 0;
	m_nSizeY = 0;

	m_pFields = NULL;

	LoadFromFile(szFilename);
}

////////////////////////////////////////////////////////////////////
bool CCMinefield::LoadFromFile(const char* szFilename)
{
	if(m_pFields)
		free(m_pFields);

	FILE* fIn = fopen(szFilename, "r");
	if(!fIn)
	{
			m_nSizeX = 0;
			m_nSizeY = 0;

			m_pFields = NULL;

			return false;
	}

	int nX, nY;
		
	int nRead = fscanf(fIn, "%d%d\n", &nX, &nY);
	if( (nRead == EOF) || (nRead < 2))
	{
		fclose(fIn);
		return false;
	}

	m_nSizeX = nX;
	m_nSizeY = nY;

	if((m_nSizeX <= 0) || (m_nSizeY <= 0))
	{
		fclose(fIn);
		return false;
	}

	m_pFields = (FIELDVAL*) malloc(nX * nY * sizeof(FIELDVAL) );

	char cFieldValue;
	for(int i = 0; i < nX; i++)
	{
		for(int j = 0; j < nY; j++)
		{
			nRead = fscanf(fIn, "%c ", &cFieldValue);
			if( (nRead == EOF) || (nRead == 0))
			{
				free(m_pFields);
				m_pFields = NULL;
				fclose(fIn);

				return false;
			}

			*(m_pFields + j + (i * nY) ) = GetFieldVal(cFieldValue);
		}
		fscanf(fIn, "\n");
	}

	fclose(fIn);

	return true;
}

////////////////////////////////////////////////////////////////////
void CCMinefield::DumpFields(COORD* Fields, 
							int nFields,
							const char* szFilename /*= NULL*/,
							int nSel /*= SEL_ALL_FIELDS*/,
							bool bAxes /*= true*/)
{
	FILE *fOut;
	if(!szFilename)
		fOut = stdout;
	else
	{
		fOut = fopen(szFilename, "w");
		if(!fOut)
			fOut = stdout;
	}

	if(bAxes)
	{
		fprintf(fOut, "   0                   1                   2                   3                   4                   5                   6                   7              \n");
		fprintf(fOut, "   0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7\n");
		fprintf(fOut, "--------------------------------------------------------------------------------------------------------------------------------------------------------------\n");
	}

	FIELDVAL FieldVal;

	// prohazujeme sou�adnice; do konzole se vypisuje v�sledek p�eh�zen�
	for(int j = 0; j < m_nSizeY; j++)
	{
		if(bAxes)
			fprintf(fOut, "%2d|", j);

		for(int i = 0; i < m_nSizeX; i++)
		{
			// vyhled�v�n� hledan�ch sou�adnic; metoda ka�d� s ka�d�m
			bool bFound = false;
			for(int k = 0; k < nFields; k++)
			{
				if( ((Fields + k)->nX == i) && (Fields + k)->nY == j )
				{
					bFound = true;
					break;
				}
			}

			if(bFound)
			{
				GetField(i, j, &FieldVal);
				if(InSelection(FieldVal, nSel))
					fprintf(fOut, "%c ",  GetChar(FieldVal));
				else
					fprintf(fOut, "%c ",  ' ');
			}
			else
				fprintf(fOut, "%c ",  ' ');
		}
		fprintf(fOut, "\n");
	}
	fprintf(fOut, "\n");
	
	if(szFilename)
		fclose(fOut);
}

////////////////////////////////////////////////////////////////////
void CCMinefield::DumpFields(CItemPool* Fields, 
							const char* szFilename /*= NULL*/,
							int nSel /*= SEL_ALL_FIELDS*/,
							bool bAxes /*= true*/)
{
	COORD** pCoords = (COORD**) malloc(Fields->GetCount() * sizeof(COORD*));
	for(int i = 0; i < (int) Fields->GetCount(); i++)
		*(pCoords + i) = (COORD*) Fields->GetItem(i);

	DumpFields(*pCoords, Fields->GetCount(), szFilename, nSel, bAxes);

	free(pCoords);
}

////////////////////////////////////////////////////////////////////
void CCMinefield::DumpAllFields(const char* szFilename, 
							   int nSel /*= SEL_ALL_FIELDS*/,
							   bool bAxes /*= true*/)
{
	// ud�l�me si seznam pol�, kter� chceme vypsat (tj, v�echna pole)
	COORD* pAllCoord = (COORD*) malloc(m_nSizeX * m_nSizeY * sizeof(COORD));
	for(int i = 0; i < m_nSizeX ; i++)
		for(int j = 0; j < m_nSizeY; j++)
		{
			(pAllCoord + j + m_nSizeY * i)->nX = i;
			(pAllCoord + j + m_nSizeY * i)->nY = j;
		}

	DumpFields(pAllCoord, m_nSizeX * m_nSizeY, szFilename, nSel, bAxes);

	free(pAllCoord);
}

////////////////////////////////////////////////////////////////////
bool CCMinefield::GetField(int nX, int nY, FIELDVAL* pField) 
{
	if( (nX < 0) || (nX >= m_nSizeX) )
		return false;

	if( (nY < 0) || (nY >= m_nSizeY) )
		return false;

	 *pField = *(m_pFields + (nX * m_nSizeY) + nY);
	 return true;
}

////////////////////////////////////////////////////////////////////
bool CCMinefield::SetField(int nX, int nY, FIELDVAL FieldVal) 
{
	if( (nX < 0) || (nX >= m_nSizeX) )
		return false;

	if( (nY < 0) || (nY >= m_nSizeY) )
		return false;

	*(m_pFields + (nX * m_nSizeY) + nY) = FieldVal;
	return true;
}

////////////////////////////////////////////////////////////////////
// nahrad� skryt� pole zadanou hodnotou
bool CCMinefield::RevealField(int nX, int nY, FIELDVAL RevValue)
{
	FIELDVAL ActVal;
	GetField(nX, nY, &ActVal);

	if(!InSelection(ActVal, INVISIB_FLAG | CANNOT_BE_MINE_FLAG) )
		return false;

	if(!InSelection(RevValue, SEL_NUM_FIELDS) )
		return false;

	int nAroundMines;
	free(GetAroundFields(&nAroundMines, nX, nY, SEL_MINE_FIELDS));

	if(nAroundMines > RevValue)
		return false;

	return SetField(nX, nY, RevValue - nAroundMines);
}

////////////////////////////////////////////////////////////////////
COORD* CCMinefield::GetAroundFields(int* p_nFields, int nX, int nY,
				int fSel /*= SEL_ALL_FIELDS*/, int nDistance /*= INDEX_DISTANCE_1*/)
{
	COORD CenterCoord = {nX, nY};
	return GetAroundFields(p_nFields, CenterCoord, fSel, nDistance);
}

////////////////////////////////////////////////////////////////////
// vr�t� okoln� pole podle filtru; pam�t se uvolnuje extern�
COORD* CCMinefield::GetAroundFields(int* p_nFields, COORD CenterCoord,
				int fSel /*= SEL_ALL_FIELDS*/, int nDistance /*= INDEX_DISTANCE_1*/)
{
	*p_nFields = 0;
	COORD* pFields = 
		(COORD*) malloc(MAX_AROUNDFIELDS[nDistance] * sizeof(COORD));
	
	if(!pFields)
	{
		printf("GetAroundFields - probl�my s alokac�");
		return NULL;
	}
	
	// vynech�me st�ed, pokud je to pot�eba
	int i = fSel & CENTRE_FLAG ? 0 : 1;

	FIELDVAL FieldVal;
	COORD FieldCoord;
	for(; i < MAX_AROUNDFIELDS[nDistance]; i++)
	{
		FieldCoord.nX = CenterCoord.nX + AROUND_FIELDS_INDEXES[i].nX;
		FieldCoord.nY = CenterCoord.nY + AROUND_FIELDS_INDEXES[i].nY;

		if(!GetField(FieldCoord, &FieldVal))
			continue;

		// pokud na dan� pol��ko spad� maska, tak ho zap�eme do pole
		if(InSelection(FieldVal, fSel) != 0)
		{
			*(pFields + *p_nFields) = FieldCoord;
			(*p_nFields)++;
		}
	}

	return pFields;
}

////////////////////////////////////////////////////////////////////
// v�b�rov� funkce
int CCMinefield::InSelection(FIELDVAL Value, int fSel)
{
	switch(Value)
	{
	case FV_NUM_0:
		return fSel & ZERO_FLAG;

	case FV_MINE:
		return fSel & MINE_FLAG;

	case FV_HIDDEN:
		return fSel & INVISIB_FLAG;

	case FV_CANNOT_BE_MINE:
		return fSel & CANNOT_BE_MINE_FLAG;

	case FV_UNKNOWN:
		return 0;
	
	default:
		{
			if( (Value > FV_NUM_0) && (Value <= FV_NUM_8) )
				return fSel & NONZERO_NUM_FLAG;
			else
			{
				printf("Chyba, funkce InSelection dostala nedefinovanou hodnotu.");
				return 0;
			}
		}
	}
}

////////////////////////////////////////////////////////////////////
// ov���, jestli m��e na zadan�m skryt�m poli b�t mina
bool CCMinefield::CheckForMine(COORD Coord)
{
	FIELDVAL FieldVal;
	GetField(Coord, &FieldVal);
	if(FieldVal != FV_HIDDEN)
		return false;

	// o�et�en� p��padu, kdy se nach�z�me uprost�ed skryt�ch pol�
	int nNonInvisible;
	free(GetAroundFields(&nNonInvisible, Coord, SEL_ALL_FIELDS & ~INVISIB_FLAG));
	if(nNonInvisible == 0)
		return true; //

	// ur�en� zkouman� oblasti
	CItemPool AffectedFields = CItemPool(sizeof(COORD));
	GetAffectedFields(&AffectedFields, Coord);

	// ur��me z�ejm� pole, na kter�ch mus� a nesm� b�t mina (urychlen�)
	CItemPool MinedFields = CItemPool(sizeof(COORD));
	CItemPool ClearFields = CItemPool(sizeof(COORD));

	FindApparentFields(&MinedFields, &ClearFields, &AffectedFields);
	if(!GetField(Coord, &FieldVal))
		printf("Funkce checkForMine dostala spatne souradnice.\n");

	if(FieldVal == FV_CANNOT_BE_MINE)
		return false;
	
	if(FieldVal == FV_MINE)
		return true; //

	// d�le pokra�ujeme hrubou silou
	// polo��me minu na dan� pole a sna��me se nal�zt spr�vn� rozm�st�n�
	MinedFields.AddItem((unsigned char*) &Coord);

	bool bGoBack;

	unsigned long nBackDeep;
	return CheckFieldsForMines(&MinedFields, &AffectedFields,
		&bGoBack, &nBackDeep); //

}

////////////////////////////////////////////////////////////////////
// Zjist�, zda-li na dan�m m�st� m��e b�t �isto
// -> vrac� false, kdy� na dan�m m�st� mus� b�t mina
bool CCMinefield::CheckForClear(COORD Coord)
{
	FIELDVAL CoordVal;
	GetField(Coord, &CoordVal);
	if( (CoordVal == FV_MINE) || (CoordVal == FV_UNKNOWN))
		return false;

	if(CoordVal != FV_HIDDEN)
		return true;

	if(PutMine(Coord))
		PickUpMine(Coord);
	else
		return true;

	SetField(Coord, FV_CANNOT_BE_MINE);

	int nAffected;
	COORD* pAffected = GetAroundFields(&nAffected, Coord, SEL_AROUND_NONZERO_NUM_FIELDS);
	if(nAffected == 0)
	{
		//��dn� ��slo okolo
		SetField(Coord, FV_HIDDEN);
		free(pAffected);

		return true;
	}
	
	// Vyberu si 1 affectedField a zkus�m okolo n�j n�jak rozm�stit miny
	int nInvisible;
	COORD* pInvisible;
	for(int i = 0; i < nAffected; i++)
	{
		pInvisible = GetAroundFields(&nInvisible, *(pAffected + i), SEL_INVISIB_FIELDS);

		if(nInvisible != 0)
			break;
		else
			free(pInvisible);
	}
	free(pAffected);

	if(nInvisible == 0)
	{
		//na�li se nenulov� ��sla, kter� maj� pouze 1 skryt� pole
		SetField(Coord, FV_HIDDEN);
		free(pInvisible);

		return false;
	}

	// Prohled�v�me celou hern� plochu pro spr�vn� rozm�st�n�
	// Jestli�e jsou v�echny varianty nemo�n�, pak zde mus� b�t mina
	bool bValidSit;
	for(int i = 0; i < nInvisible; i++)
	{
		// test, jestli jsou v�echna pole okolo skryt�
		int nNonInvisible;
		free(GetAroundFields(&nNonInvisible, *(pInvisible + i), SEL_ALL_FIELDS & ~INVISIB_FLAG));
		if(nNonInvisible == 0)
		{
			SetField(Coord, FV_HIDDEN);
			free(pInvisible);

			return true;
		}

		CItemPool MinedFields = CItemPool(sizeof(COORD));
		CItemPool ClearFields = CItemPool(sizeof(COORD));
		
		// ur�en� zkouman� oblasti
		CItemPool AffectedFields = CItemPool(sizeof(COORD));
		GetAffectedFields(&AffectedFields, *(pInvisible + i));

		MinedFields.AddItem((unsigned char*) (pInvisible + i));

		bool bGoBack;
		unsigned long nBackDeep;
		bValidSit = CheckFieldsForMines(&MinedFields, &AffectedFields,
			&bGoBack, &nBackDeep);

		COORD CleanCoord;
		for(int i = 0; i < (int) MinedFields.GetCount(); i++)
		{
			CleanCoord = *((COORD*) MinedFields.GetItem(i));
			PickUpMine(CleanCoord);
		}

		for(int i = 0; i < (int) ClearFields.GetCount(); i++)
		{
			CleanCoord = *((COORD*) ClearFields.GetItem(i));
			SetField(CleanCoord, FV_HIDDEN);
		}

		if(bValidSit)
			break;
	}

	SetField(Coord, FV_HIDDEN);
	free(pInvisible);

	return bValidSit;
}

////////////////////////////////////////////////////////////////////
void CCMinefield::GetAffectedFields(CItemPool* AffectedFields, COORD Coord)
{
	// bitov� pole, pro uchov�n� informace, zda byla ji� pozice prohled�na
	CHECKED_FIELDS pCheckedFields = 
		(CHECKED_FIELDS) malloc(m_nSizeX * m_nSizeY * sizeof(CHECKED_FIELDS));
	memset(pCheckedFields, 0, m_nSizeX * m_nSizeY);
	
	int nAffectFields;
	COORD* pAffectFields = GetAroundFields(&nAffectFields, Coord,
		SEL_AROUND_NONZERO_NUM_FIELDS, INDEX_DISTANCE_2);

	long nCheckedItems = 0;
	int nCheckFieldIndex;
	for(;;)
	{
		for(int i = 0; i < nAffectFields; i++)
		{
			nCheckFieldIndex = (pAffectFields + i)->nY + (pAffectFields + i)->nX * m_nSizeY;
			if(*(pCheckedFields + nCheckFieldIndex))
				continue;

			AffectedFields->AddItem((unsigned char*) (pAffectFields + i));

			*(pCheckedFields + nCheckFieldIndex) = true;
		}
		free(pAffectFields);
		
		// skon��me, jestli jsme v posledn�m kroku nic nep�idali
		if( nCheckedItems == AffectedFields->GetCount() )
			break;

		pAffectFields = GetAroundFields(&nAffectFields,
								*((COORD*) AffectedFields->GetItem(nCheckedItems)),
								SEL_AROUND_NONZERO_NUM_FIELDS, 
								INDEX_DISTANCE_2);

		nCheckedItems++;
	}
	
	free(pCheckedFields);
}

////////////////////////////////////////////////////////////////////
bool CCMinefield::TestCheckFieldsForMines(COORD Coord, CItemPool* AffectedFields)
{
//	CItemPool AffectedFields	= CItemPool(sizeof(COORD));
	CItemPool MinedFields		= CItemPool(sizeof(COORD));
	CItemPool ClearFields		= CItemPool(sizeof(COORD));

//	if(!AffectedFieldsFromFile(szAffFieldsFn))
//		return false;

	MinedFields.AddItem((unsigned char*) &Coord);

	//breakp2 = true;
	DumpAllFields("chyba.txt");


	bool bGoBack;
	unsigned long nBackDeep;
	bool bRes = CheckFieldsForMines(&MinedFields, AffectedFields,
		&bGoBack, &nBackDeep);
//	breakp2 = false;

	return bRes;
}

////////////////////////////////////////////////////////////////////
// rekurzivn� vyhled�v� mo�n� rozm�st�n� min 
// MinedFields - sou�adnice rozm�st�n�ch min
// AffectedFields - oblast, na kterou se soust�ed�me
// nLayedMines - po�et rozm�st�n�ch min
// BackSeq - fronta, obsahuj�c� sekvenci, do kter� hloubky se m�me vr�tit;
//			 se�azen� vzestupn�, v�dy obsahuje 1 prvek(nejv�t�� hloubka)
bool CCMinefield::CheckFieldsForMines(CItemPool* MinedFields, CItemPool* AffectedFields,
				bool* bGoBack, unsigned long* pBackDeep)
{
	*bGoBack = false;

	unsigned long nRecDeep = MinedFields->GetCount() - 1;
	COORD MinedField = *((COORD*) MinedFields->GetItem(nRecDeep));

	FIELDVAL MinedVal;
	GetField(MinedField, &MinedVal);
	if(MinedVal != FV_HIDDEN)
		return false;

	// polo��me minu a jestli bude n��emu vadit, tak se vr�t�me
	if(!PutMine(MinedField))
		return false;

	// a zkou��me p�idat dal�� minu
	FIELDVAL FieldVal;
	COORD* pNextAff;
	COORD* pInvisibFields = NULL;
	bool bAllZeroes = true;
	bool bResult;

	for(int i = 0; i < (int) AffectedFields->GetCount(); i++)		
	{
		// v�echny ��seln� pole, kter� by mohly b�t ovlivn�ny p�id�n�m miny
		// hodnota n�kter�ch z nich mohla b�t p�ed chv�l� sn�ena na 0
		pNextAff = (COORD*) AffectedFields->GetItem(i);
		
		// k okol� nuly u� nic p�id�vat nebudeme
		GetField(*pNextAff, &FieldVal);
		if(FieldVal == FV_NUM_0)
			continue;
		bAllZeroes = false;

		// m�me ��seln� pole, ke kter�mu se budeme sna�it p�idat minu
		// postupn� se ji pokus�me p�idat na v�echny okoln� skryt� pole
		int nInvisibFields;
		pInvisibFields = GetAroundFields(&nInvisibFields, *pNextAff,
					SEL_AROUND_INVISIB_FIELDS);

		for(int j = 0; j < nInvisibFields; j++)
		{
			MinedFields->AddItem( (unsigned char*) (pInvisibFields + j) );

			if(CheckFieldsForMines(MinedFields, AffectedFields, bGoBack, pBackDeep))
			{
				// n�kde v hlouby se na�lo spr�vn� rozm�st�n�
				MinedFields->RemoveLast();

				free(pInvisibFields);

				bResult = true;
				goto CleanUp;
			}

			MinedFields->RemoveLast();

			if(!(*bGoBack))
				continue;

			// n�kde v hlouby vadila *pBackSeq-t� mina, tak�e se k n� vr�t�me
			//unsigned long nLastBackSeqMine = *( BackSeq->GetItem(BackSeq->GetCount() - 1));
			unsigned long nLastBackSeqMine = *pBackDeep;
			if(nRecDeep > nLastBackSeqMine)
			{
				free(pInvisibFields);

				bResult = false;
				goto CleanUp;
			}

			*pBackDeep = nRecDeep;

			*bGoBack = false;
		}
		free(pInvisibFields);

		// prohledali jsme v�echny mo�nosti a nic, horn� rekurze to ud�lala �patn�
		FindBadRecursionDeep(MinedFields, *pNextAff, pBackDeep);
		*bGoBack = true;

		bResult = false;
		goto CleanUp;
	}

	// u� nebylo kam p�id�vat; jestli jsou v�ude v AffectedFields nuly,
	// nalezli jsme spr�vn� rozm�st�n�
	bResult = bAllZeroes;

	if(bResult && m_bDevilSolution)
	{
		m_pDevilSolution = (FIELDVAL*) malloc(m_nSizeX * m_nSizeY * sizeof(FIELDVAL));
		memcpy(m_pDevilSolution, m_pFields, m_nSizeX * m_nSizeY * sizeof(FIELDVAL));
	}

CleanUp:

	// je�t� navr�t�me pole do p�vodn�ho stavu
	if(!PickUpMine(MinedField))
	{
		printf("funkce CheckFieldsForMines - PickUpMine nevyzvedlo minu(u� tam nebyla.)");
	}

	return bResult;
}

////////////////////////////////////////////////////////////////////
// vyhled� miny, jejich� ubr�n�m je mo�n� polo�it n�kam k AffField minu
// a se�ad� je vzestupn� podle hloubky rekurze, kde byla mina polo�ena
bool CCMinefield::FindBadRecursionDeep(CItemPool* MinedFields, COORD AffField, unsigned long* pBackDeep)
{

	*pBackDeep = 0;
	int nLayedMines = MinedFields->GetCount();

	int i;
	for(i = 0; i < nLayedMines; i++)
	{
		int nLastIndex = (nLayedMines - 1) - i;
		COORD* pMineCoord = (COORD*) MinedFields->GetItem( nLastIndex );
		PickUpMine( *pMineCoord );

		if(CheckOneField(AffField))
		{
			// tato mina vadila, ulo��me si hloubku, kde byla polo�ena
			*pBackDeep = nLayedMines - 1 - i;

			break;
		}
	}

	// odebran� miny polo��me zp�tky
	for(int j = *pBackDeep; j < nLayedMines; j++)
	{
		int nLastIndex = j;
		COORD* pMineCoord = (COORD*) MinedFields->GetItem( nLastIndex );

		if(!PutMine( *pMineCoord ))
			printf("FindBadRecursionDeep �patn� index\n");
	}

	return true;
}

////////////////////////////////////////////////////////////////////
// zjist�, zda je mo�n� um�stit miny na okol� dan�ho pole
bool CCMinefield::CheckOneField(COORD Coord)
{
	FIELDVAL FieldNum;
	GetField(Coord, &FieldNum);
	if(!InSelection(FieldNum, SEL_NUM_FIELDS))
	{
		printf("funkce CheckOneField dostala �patn� argument.\n");
		return false;
	}

	int nInvFields;
	COORD* pInvFields = GetAroundFields(&nInvFields, Coord, SEL_AROUND_INVISIB_FIELDS);

	bool bResult = CheckOneField(Coord, pInvFields, nInvFields);
	free(pInvFields);

	return bResult;
}

////////////////////////////////////////////////////////////////////
// zjist� zda je mo�n� n�jak okolo ��seln�ho pole rozm�stit miny
// rekurzivn� volan�
bool CCMinefield::CheckOneField(COORD Coord, COORD* pInvFields, int nInvFields)
{
	FIELDVAL FieldNum;
	for(int i = 0; i < nInvFields; i++)
		if(PutMine( *(pInvFields + i)) )
		{
			GetField(Coord, &FieldNum);
			if(FieldNum == FV_NUM_0)
			{
				PickUpMine( *(pInvFields + i) );
				return true;
			}

			if(CheckOneField(Coord))
			{
				PickUpMine( *(pInvFields + i) );
				return true;
			}

			PickUpMine( *(pInvFields + i) );
		}

	return false;
}

////////////////////////////////////////////////////////////////////
// rozm�st� na zkoumanou oblast z�ejm� miny a odhal� pole, kde miny
// nem��ou b�t
void CCMinefield::FindApparentFields(CItemPool* MinedFields,
								  CItemPool* ClearFields,
								  CItemPool* AffectedFields)
{
	int nAffectedFields = AffectedFields->GetCount();

	bool bChange;
	bool bGoBack;
	int nInvisible;
	COORD* pInvisible;
	COORD* pAffected;
	FIELDVAL FieldVal;

	do
	{
		bChange = false;
		
		// vyhled�me pole, na kter�ch mus� b�t mina
		for(int i = 0; i < nAffectedFields; i++)
		{
			pAffected = (COORD*) AffectedFields->GetItem(i);
			pInvisible = GetAroundFields(&nInvisible,
									*pAffected,
									SEL_AROUND_INVISIB_FIELDS);			
		
			GetField(*pAffected, &FieldVal);
			if( (nInvisible == FieldVal) && (nInvisible != 0) )
				for(int j = 0; j < nInvisible; j++)
					if( PutMine(*(pInvisible + j)) )
					{
						MinedFields->AddItem((unsigned char*) (pInvisible + j));
						bChange = true;
					}

			free(pInvisible);
		}

		// vyhled�me pole, na kter�ch nesm� b�t mina s p�i 
		// vyhled�v�n� na omezen� oblasti
		for(int i = 0; i < nAffectedFields; i++)
		{

			pAffected = (COORD*) AffectedFields->GetItem(i);

			// vytvo��me omezenou oblast pro prohled�v�n�
			CItemPool LimitedAreaAff = CItemPool(sizeof(COORD));
			int nLimited;
			COORD* pLimited = GetAroundFields(&nLimited,
									*pAffected,
									SEL_NONZERO_NUM_FIELDS);

			for(int j = 0; j < nLimited; j++)
				LimitedAreaAff.AddItem((unsigned char*) (pLimited + j));
			free(pLimited);

			pInvisible = GetAroundFields(&nInvisible,
									*pAffected,
									SEL_AROUND_INVISIB_FIELDS);			

			for(int j = 0; j < nInvisible; j++)
			{
				unsigned long nBackDeep;
				CItemPool TempMinedFields = CItemPool(sizeof(COORD));
				TempMinedFields.AddItem((unsigned char*) (pInvisible + j));
				if(!CheckFieldsForMines(&TempMinedFields,
										&LimitedAreaAff,
										&bGoBack,
										&nBackDeep) )
				{
					SetField(*(pInvisible + j), FV_CANNOT_BE_MINE);
					ClearFields->AddItem((unsigned char*) (pInvisible + j));

					MinedFields->RemoveLast();

					bChange = true;

					continue;
				}
				MinedFields->RemoveLast();
			}

			free(pInvisible);
		}
	}
	while(bChange);
}

////////////////////////////////////////////////////////////////////
bool CCMinefield::PutMine(COORD MineCoord)
{
	FIELDVAL FieldVal;
	GetField(MineCoord, &FieldVal);
	if(FieldVal != FV_HIDDEN)
		return false;

	// bude mina n��emu vadit?
	int nNumFields;
	COORD* pNumFields;
	pNumFields = GetAroundFields(&nNumFields, MineCoord, SEL_AROUND_NUM_FIELDS);

	for(int i = 0; i < nNumFields; i++)
	{
		GetField(*(pNumFields + i), &FieldVal);
		if(FieldVal == FV_NUM_0)
		{
			//mina sem nejde polo�it
			free(pNumFields);
			return false;
		}
	}

	// sn��me okolo hodnoty pol�
	SetField(MineCoord, FV_MINE);
	for(int i = 0; i < nNumFields; i++)
	{
		GetField(*(pNumFields + i) ,&FieldVal);
		FieldVal--;

		SetField(*(pNumFields + i), FieldVal );
	}

	free(pNumFields);			
	return true;
}

////////////////////////////////////////////////////////////////////
bool CCMinefield::PickUpMine(COORD MineCoord)
{
	FIELDVAL FieldVal;
	GetField(MineCoord, &FieldVal);
	if(FieldVal != FV_MINE)
		return false;

	int nNumFields;
	COORD* pNumFields = GetAroundFields(&nNumFields, MineCoord, SEL_AROUND_NUM_FIELDS);			

	for(int i=0; i < nNumFields; i++)
	{
		GetField(*(pNumFields + i), &FieldVal);
		if(FieldVal == FV_NUM_8)
		{
			// chyba v zad�n�
			free(pNumFields);
			return false;
		}
	}

	// zv���me okolo hodnoty pol�
	SetField(MineCoord, FV_HIDDEN);
	for(int i = 0; i < nNumFields; i++)
	{
		GetField(*(pNumFields + i) ,&FieldVal);
		FieldVal++;

		SetField(*(pNumFields + i), FieldVal );
	}

	free(pNumFields);			
	return true;
}

////////////////////////////////////////////////////////////////////
int CCMinefield::SolveOneField(int nX, int nY)
{
	COORD Coord;
	Coord.nX = nX;
	Coord.nY = nY;

	return SolveOneField(Coord);
}

////////////////////////////////////////////////////////////////////
int CCMinefield::SolveOneField(COORD Coord)
{
	FIELDVAL FieldVal;
	GetField(Coord, &FieldVal);
	switch(FieldVal)
	{
	case FV_MINE:
		return SOLVE_MUST_BE_MINE;

	case FV_CANNOT_BE_MINE:
		return SOLVE_CANNOT_BE_MINE;
	
	case FV_HIDDEN:
		{
			// zjist�me, jestli na poli nem��e b�t mina
			if(!CheckForMine(Coord))
			{
				SetField(Coord, FV_CANNOT_BE_MINE);
				return SOLVE_CANNOT_BE_MINE;
			}

			// zjist�me, jestli na poli mus� b�t mina
			if(!CheckForClear(Coord))
			{
				PutMine(Coord);
				return SOLVE_MUST_BE_MINE;
			}
		
		}
	}

	return SOLVE_NO_DECISION;
}

////////////////////////////////////////////////////////////////////
// Hled� mo�nost spr�vn�ho rozm�st�n� min, tak aby vyhovovala sou�asn� situaci
FIELDVAL* CCMinefield::FindDevilSolution(COORD Coord)
{
	m_bDevilSolution = true;

	FIELDVAL FieldVal;
	GetField(Coord, &FieldVal);
	if(FieldVal != FV_HIDDEN)
	{
		m_bDevilSolution = false;
		return NULL;
	}

	// o�et�en� p��padu, kdy se nach�z�me uprost�ed skryt�ch pol�
	int nNonInvisible;
	free(GetAroundFields(&nNonInvisible, Coord, SEL_ALL_FIELDS & ~INVISIB_FLAG));
	if(nNonInvisible == 0)
	{
		m_bDevilSolution = false;

		FIELDVAL* result = (FIELDVAL*) malloc(m_nSizeX * m_nSizeY * sizeof(FIELDVAL));
		memcpy(result, m_pFields, m_nSizeX * m_nSizeY * sizeof(FIELDVAL));

		*(result + (Coord.nX * m_nSizeY) + Coord.nY) = FV_MINE;
		return result; 
	}

	// prozkoum�me okol� v�ech ��sel
	CItemPool AffectedFields = CItemPool(sizeof(COORD));
	GetAffectedFields(&AffectedFields, Coord);
/*
	for(int i = 0; i < m_nSizeX; i++)
		for(int j = 0; j < m_nSizeY; j++)
			if(InSelection(*(m_pFields + i * m_nSizeY) + j, SEL_NONZERO_NUM_FIELDS))
			{
				COORD c = {i, j};
				AffectedFields.AddItem((unsigned char*) &c);
			}
*/
	// polo��me minu na dan� pole a sna��me se nal�zt spr�vn� rozm�st�n�
	CItemPool MinedFields = CItemPool(sizeof(COORD));
	MinedFields.AddItem((unsigned char*) &Coord);

	bool bGoBack;

	unsigned long nBackDeep;
	if(CheckFieldsForMines(&MinedFields, &AffectedFields, &bGoBack, &nBackDeep))
	{
		m_bDevilSolution = false;
		return m_pDevilSolution;
	}

	m_bDevilSolution = false;
	return NULL;
}

////////////////////////////////////////////////////////////////////
// �e�� zadanou situaci, pokud nenalezne n�jakou zm�nu (nelze rozhodnout
// o minovosti nebo neminovosti n�jak�ho pole) vr�t� false
bool CCMinefield::Solve(CItemPool* MinedFields, CItemPool* ClearFields)
{
	bool bChange = false;

	COORD Coord;
	FIELDVAL FieldVal;
	for(Coord.nX = 0; Coord.nX < m_nSizeX ; Coord.nX++)
		for(Coord.nY = 0; Coord.nY < m_nSizeY; Coord.nY++)
		{
			GetField(Coord, &FieldVal);
			if(FieldVal != FV_HIDDEN)
			{
				// narazili jsme na u� d��ve polo�enou minu, ��dn� zm�na
				if(FieldVal == FV_MINE)
					MinedFields->AddItem(((unsigned char*) &Coord));

				if(FieldVal == FV_CANNOT_BE_MINE)
				{
					bChange = true;
					ClearFields->AddItem((unsigned char*) &Coord);
				}

				continue;
			}

			// zjist�me, jestli na poli nem��e b�t mina
			if(!CheckForMine(Coord))
			{
				SetField(Coord, FV_CANNOT_BE_MINE);
				ClearFields->AddItem((unsigned char*) &Coord);
				bChange = true;

				continue;
			}

			// zjist�me, jestli na poli mus� b�t mina
			if(!CheckForClear(Coord))
			{
				PutMine(Coord);
				MinedFields->AddItem(((unsigned char*) &Coord));
				bChange = true;

				continue;
			}
	}
	return bChange;
}
