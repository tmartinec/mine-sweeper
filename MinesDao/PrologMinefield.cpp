#include "PrologMinefield.h"

using namespace System::IO;
using namespace System::Text;
using namespace System::Reflection;
using namespace System::Threading;

enum class PROLOG_RESULT
{
	PROLOG_TRUE,
	PROLOG_FALSE,
	PROLOG_ERROR
};

ref class CGlobals 
{
public:
	static StringBuilder^ mBuffer = gcnew StringBuilder();
	static Process^ mRunnedProlog = gcnew Process();
	static bool mPrologReady = false;
	static PROLOG_RESULT mPrologResult;
};

static void PrintAllUsedCommands()
{
	File::WriteAllText("prologCommands.txt", CGlobals::mBuffer->ToString());
}

void CPrologMinefield::Init(int nSizeX, int nSizeY, FIELDVAL FieldsVal, String^ prologPath)
{
	String^ appPath = Assembly::GetCallingAssembly()->CodeBase;
	String^ appDir = Path::GetDirectoryName(appPath);
	appDir = appDir->Replace("file:\\", "");

	String^ prologScriptPath = "'" + appDir + "\\minesSolver.pl'";
	prologScriptPath = prologScriptPath->Replace("\\", "/");

	StartProlog(prologPath, prologScriptPath);

	Char^ fieldChar = gcnew Char(GetChar(FieldsVal));

	for (int i = 0; i < nSizeX; i++)
	{
		for (int j = 0; j < nSizeY; j++)
		{
			String^ predicate = "asserta(minefield((" + i + "," + j + ")," + fieldChar + ")).";
			QueryPredicate(predicate);
		}
	}

//	QueryPredicate("printMinefield.");
}

void CPrologMinefield::StartProlog(String^ prologExec, String^ scriptFilename)
{
	CGlobals::mRunnedProlog = gcnew Process();

	ProcessStartInfo^ startInfo = gcnew ProcessStartInfo(prologExec);
 	startInfo->RedirectStandardInput = true;
    startInfo->RedirectStandardOutput = true;
	startInfo->RedirectStandardError = true;
	startInfo->WindowStyle = ProcessWindowStyle::Hidden;
    startInfo->CreateNoWindow = true;
    startInfo->UseShellExecute = false;

	CGlobals::mRunnedProlog->StartInfo = startInfo;

	CGlobals::mRunnedProlog->Start();

	CGlobals::mRunnedProlog->OutputDataReceived += gcnew DataReceivedEventHandler(RecievePrologLine);

	CGlobals::mRunnedProlog->BeginOutputReadLine();

	CGlobals::mPrologReady = true;

	QueryPredicate("consult(" + scriptFilename + ").", false);

	String^ initString = "";
	while (!initString->Equals("true."))
	{
		initString = CGlobals::mRunnedProlog->StandardError->ReadLine();
	}
}

int mReadOutputLines;

void CPrologMinefield::PrintPrologMinefield(int lines)
{
	mReadOutputLines = 0;

	QueryPredicate("printMinefield.");

	// there is also an empty line, so that is + 1
	while (mReadOutputLines < lines + 1)
	{
		Thread::CurrentThread->Sleep(500);
	}
}

void CPrologMinefield::RecievePrologLine(Object^ sender, System::Diagnostics::DataReceivedEventArgs^ e)
{
	mReadOutputLines++;

	if (e->Data != nullptr)
	{
		if(e->Data->IndexOf("Assert failed") != -1)
		{
			Debug::Fail(e->Data);
		}
	}

	Debug::WriteLine("Prolog O: " + e->Data);
}

void CPrologMinefield::RecievePrologErrorLine(Object^ sender, System::Diagnostics::DataReceivedEventArgs^ e)
{
}

int CPrologMinefield::SolveOneField(int nX, int nY)
{
	String^ coord = "(" + nX + "," + nY + ")";
	QueryPredicate("solveOneField(" + coord + ").");

	if (QueryPredicate("coveredField(" + coord +")."))
	{
		return SOLVE_NO_DECISION;
	}

	if (QueryPredicate("minedField(" + coord +")."))
	{
		return SOLVE_MUST_BE_MINE;
	}

	if (QueryPredicate("minefield(" + coord +",c)."))
	{
		return SOLVE_CANNOT_BE_MINE;
	}

	Debug::Fail("Unknown minefield content");
	return 0;
}

String^ readLine();
String^ readLine()
{
	StringBuilder^ builder = gcnew StringBuilder();
		
	while (true)
	{
		Char c = (Char) CGlobals::mRunnedProlog->StandardError->Read();

		if (c == '\r')
		{
			Char endOfLine = CGlobals::mRunnedProlog->StandardError->Peek();
			if (endOfLine == '\n')
			{
				CGlobals::mRunnedProlog->StandardError->Read();
			}

			break;
		}

		if (c == '\n')
		{
			break;
		}

		builder->Append(c);

		if (c == ' ')
		{
			int value = CGlobals::mRunnedProlog->StandardError->Peek();
			if (value == -1)
			{
				break;
			}
		}
	}

	return builder->ToString();
}

void CPrologMinefield::ReadAnswer()
{
	CGlobals::mPrologReady = false;

	while( !CGlobals::mPrologReady )
	{
		String^ answer = readLine();
		Debug::WriteLine("Prolog E: " + answer);

		if (answer != nullptr)
		{
			if (answer->Contains("true")) 
			{
				CGlobals::mPrologResult = PROLOG_RESULT::PROLOG_TRUE;
			}

			if (answer->Contains("false")) 
			{
				CGlobals::mPrologResult = PROLOG_RESULT::PROLOG_FALSE;
			}

			if (answer->EndsWith(" ")) 
			{
				CGlobals::mRunnedProlog->StandardInput->WriteLine();
				CGlobals::mPrologReady = true;
			}

			if (answer->EndsWith("."))
			{
				CGlobals::mPrologReady = true;
			}

			if (answer->Contains("ERROR:")) 
			{
				CGlobals::mPrologReady = true;
				CGlobals::mPrologResult = PROLOG_RESULT::PROLOG_ERROR;
			}
		}
	}

	String^ emptyLine = readLine();

	Debug::WriteLine("Prolog E: " + emptyLine);
	Debug::Assert(emptyLine->Equals(""));
}

int CPrologMinefield::SolveOneField(COORD Coord)
{
	return SolveOneField(Coord.nX, Coord.nY);
}

bool CPrologMinefield::RevealField(int nX, int nY, FIELDVAL RevValue)
{
	if (RevValue >= 9)
	{
		return false;
	}

	String^ coord = "(" + nX + "," + nY + ")";
	Char^ value = gcnew Char(GetChar(RevValue));

	bool canReveal = QueryPredicate("coveredField(" + coord +").");
	canReveal |= QueryPredicate("clearField(" + coord +").");
	if (!canReveal)
	{
		return false;
	}

	QueryPredicate("revealField(" + coord + "," + value + ").");

	return true;
}

bool CPrologMinefield::RevealField(COORD Coord, FIELDVAL RevValue)
{
	return RevealField(Coord.nX, Coord.nY, RevValue);
}

void CPrologMinefield::Close()
{
	CGlobals::mRunnedProlog->Close();
}

//void CPrologMinefield::WaitForPrologReady()
//{
//
//}

bool CPrologMinefield::QueryPredicate(String^ predicate, bool waitForProlog)
{
	CGlobals::mBuffer->Append(predicate + "\r\n");

	CGlobals::mRunnedProlog->StandardInput->WriteLine(predicate);

	Debug::WriteLine("Prolog I: " + predicate);

	if (!waitForProlog)
	{
		return true;
	}

	ReadAnswer();

	PrintAllUsedCommands();

	return CGlobals::mPrologResult == PROLOG_RESULT::PROLOG_TRUE;
}
