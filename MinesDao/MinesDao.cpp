/*
 * File:		MinesDao.cpp
 * Date:		2. �nora 2008
 * Author:		Tom� Martinec
 * Projekt:		Z�po�tov� program z p�edm�tu Programov�n� I
 * Popis:		T��da pro �e�en� minov�ho pole.
 */

#include "MinesDao.h"

////////////////////////////////////////////////////////////////////
// vrac� hodnotu pole pro znak
FIELDVAL CMinefield::GetFieldVal(char cFieldValue)
{
	switch(cFieldValue)
	{
		case C_MINE: 
			return FV_MINE;
		case C_INVISIB:
			return FV_HIDDEN;
		default:
		{				
			if ((cFieldValue >= C_NUM_0) && (cFieldValue <= C_NUM_8))
				return cFieldValue - C_NUM_0;
			else
				return FV_UNKNOWN;
		}
	}
}

////////////////////////////////////////////////////////////////////
// vrac� znak pro odpov�daj�c� hodnotu pole
char CMinefield::GetChar(FIELDVAL FieldVal)
{
	switch(FieldVal)
	{
		case FV_MINE: 
			return C_MINE;

		case FV_HIDDEN:
			return C_INVISIB;

		case FV_CANNOT_BE_MINE:
			return 'c';

		default:
		{				
			if ((FieldVal >= FV_NUM_0) && (FieldVal <= FV_NUM_8))
				return FieldVal + C_NUM_0;
			else
				return FV_UNKNOWN;
		}
	}
}

