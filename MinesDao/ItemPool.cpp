/*
 * File:		MinesDao.cpp
 * Date:		2. �nora 2008
 * Author:		Tom� Martinec
 * Projekt:		Z�po�tov� program z p�edm�tu Programov�n� I
 * Popis:		Jednoduch� spr�vce pam�ti.
 */

#define _CRT_SECURE_NO_DEPRECATE

#include <string.h>
#include <stdio.h>

#include "ItemPool.h"

////////////////////////////////////////////////////////////////////
CMemMan::CMemMan(unsigned long nItemSize, unsigned long nBuff /* = CMEMMAN_BUFF*/)
{
	m_nMemError = E_ITEMPOOL_OK;

	m_nItemSize = nItemSize;
	m_nBuffSize = nBuff;
	m_nItems = 0;

	m_pItems = (unsigned char*) malloc(m_nBuffSize);
	if(!m_pItems)
		_get_errno(&m_nMemError);
} 

////////////////////////////////////////////////////////////////////
bool CMemMan::AddItem(unsigned char *pItem)
{
	if(!pItem)
		return false;

	while( (m_nItems + 1) * m_nItemSize >= m_nBuffSize ) 
	{
		m_nBuffSize *= 2;
		m_pItems = (unsigned char*) realloc(m_pItems, m_nBuffSize);
		if(!m_pItems)
		{
			_get_errno(&m_nMemError);
			return false;
		}
	}

	memcpy(m_pItems + (m_nItems * m_nItemSize), pItem, m_nItemSize);
	m_nItems++;

	return true;
}

////////////////////////////////////////////////////////////////////
CItemPool::CItemPool(unsigned long nItemSize,
		int (__cdecl *compare )(const void *, const void *) /*= NULL*/,
		unsigned long nBuff /* = CMEMMAN_BUFF*/) : CMemMan(nItemSize, nBuff)
{
	m_compare = compare;
} 

////////////////////////////////////////////////////////////////////
bool CItemPool::SetItem(unsigned char* pItem, unsigned long nIndex)
{
	if(!pItem)
		return false;

	if(nIndex > m_nItems)
		return false;

	memcpy(m_pItems + (nIndex * m_nItemSize), pItem, m_nItemSize);

	return true;
}

////////////////////////////////////////////////////////////////////
unsigned char* CItemPool::GetItem(unsigned long nIndex)
{
	if( (nIndex >= m_nItems) && (nIndex < 0) )
		return NULL;
	
	return m_pItems + (nIndex * m_nItemSize);
}

////////////////////////////////////////////////////////////////////
// metoda ka�d� s ka�d�m
bool CItemPool::FindItem(unsigned char* pItem, unsigned long* p_nIndex)
{
	unsigned long i;
	for(i = 0; i < m_nItems; i++)
		if(memcmp(pItem, m_pItems + (i * m_nItemSize), m_nItemSize) == 0)
		{
			*p_nIndex = i;
			return true;
		}

	return false;
}

////////////////////////////////////////////////////////////////////
bool CItemPool::ToFile(char* szFilename)
{
	FILE *fOut = fopen(szFilename,"wb");
	if(!fOut)
		return false;

	if(fwrite(m_pItems, m_nItemSize, m_nItems, fOut) != m_nItems)
	{
		fclose(fOut);	
		return false;
	}

	fclose(fOut);	
	return true;
}

////////////////////////////////////////////////////////////////////
bool CItemPool::FromFile(char* szFilename)
{
	FILE *fIn = fopen(szFilename,"rb");
	if(!fIn)
		return false;

	if(fread(m_pItems, m_nItemSize, m_nItems, fIn) != m_nItems)
	{
		fclose(fIn);	
		return false;
	}

	fclose(fIn);	
	return true;
}
