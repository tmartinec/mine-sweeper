#pragma once

#include "ItemPool.h"

////////////////////////////////////////////////////////////////////
// znaky pro vstup
const char C_NUM_0 = '0';
const char C_NUM_1 = '1';
const char C_NUM_2 = '2';
const char C_NUM_3 = '3';
const char C_NUM_4 = '4';
const char C_NUM_5 = '5';
const char C_NUM_6 = '6';
const char C_NUM_7 = '7';
const char C_NUM_8 = '8';
const char C_MINE	 = '*';
const char C_INVISIB = '?';
const char C_UNKNOWN = '!';

////////////////////////////////////////////////////////////////////
typedef int FIELDVAL;
typedef bool* CHECKED_FIELDS;

struct COORD
{
	int nX;
	int nY;
};

const int MAX_AROUNDFIELDS[]	= {1, 9, 25};
// st�ed je prvn�
const COORD AROUND_FIELDS_INDEXES[] = 
{
	{0,0},
	{-1,-1}, {-1,0}, {-1,1}, {0,-1}, {0,1}, {1,-1}, {1,0}, {1,1},
	{-2,-2}, {-2,-1}, {-2,0}, {-2,1}, {-2,2}, {-1,-2}, {-1,2}, {0,-2},
	{0,2}, {1,-2}, {1,2}, {2,-2}, {2,-1}, {2,0}, {2,1}, {2,2} 
};

////////////////////////////////////////////////////////////////////
// hodnoty pro reprezentaci pol� v pam�ti
const FIELDVAL FV_NUM_0				= 0;
const FIELDVAL FV_NUM_8				= 8;
const FIELDVAL FV_MINE				= 9;
const FIELDVAL FV_HIDDEN			= 10;
const FIELDVAL FV_CANNOT_BE_MINE	= 11;
const FIELDVAL FV_UNKNOWN			= 12;

const int SOLVE_NO_DECISION				= 0;
const int SOLVE_CANNOT_BE_MINE			= 1;
const int SOLVE_MUST_BE_MINE			= 2;

class CMinefield abstract
{
public:
	static FIELDVAL GetFieldVal(char cFieldValue);
	static char GetChar(FIELDVAL FieldVal);

	virtual void Close() abstract;

	virtual int SolveOneField(int nX, int nY) abstract; 
	virtual int SolveOneField(COORD Coord) abstract;

	virtual bool RevealField(int nX, int nY, FIELDVAL RevValue) abstract;
	virtual bool RevealField(COORD Coord, FIELDVAL RevValue) abstract;
};
