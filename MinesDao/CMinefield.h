#pragma once

#include "MinesDao.h"

////////////////////////////////////////////////////////////////////
// konstanty pro vyhled�vac� a v�b�rov� funkce
const int INDEX_DISTANCE_0	= 0;
const int INDEX_DISTANCE_1	= 1;
const int INDEX_DISTANCE_2	= 2;

const int CENTRE_FLAG			=	0x0001;	// st�ed; na typ pole nem� vliv
const int NONZERO_NUM_FLAG		=	0x0002; // ��sla, krom� nuly
const int ZERO_FLAG				=	0x0004; // nulov� ��sla
const int MINE_FLAG				=	0x0008; // miny
const int INVISIB_FLAG			=	0x0010; // neodkryt� pole
const int CANNOT_BE_MINE_FLAG	=	0x0020; // pole, na kter�ch nem��e b�t mina

const int SEL_ALL_FIELDS				= 0x003f;
const int SEL_NUM_FIELDS				= 0x0007;
const int SEL_NONZERO_NUM_FIELDS		= 0x0003;
const int SEL_MINE_FIELDS				= 0x0009;
const int SEL_INVISIB_FIELDS			= 0x0011;
const int SEL_NONZERO_FIELDS			= 0x003b;
const int SEL_CANNOT_BE_MINE_FIELDS		= 0x0020;
const int SEL_AROUND_ZERO_FIELDS		= 0x0004;
const int SEL_AROUND_INVISIB_FIELDS		= 0x0010;
const int SEL_AROUND_NUM_FIELDS			= 0x0006;
const int SEL_AROUND_NONZERO_NUM_FIELDS	= 0x0002;

////////////////////////////////////////////////////////////////////
class CCMinefield : public CMinefield
{
public:
	virtual void Init(int nSizeX, int nSizeY, FIELDVAL FieldsVal);
	void Init(int nSizeX, int nSizeY, const FIELDVAL* pFields);
	void Init(CCMinefield* Field);
	void Init(const char* szFilename);

	virtual void Close() override {}

	~CCMinefield() {if(m_pFields != NULL) free(m_pFields);}

	bool LoadFromFile(const char* szFilename);
	bool IsEmpty() {return m_pFields == NULL;}

	void DumpFields(COORD* Fields, int nFields, const char* szFilename = NULL,
		int nSel = SEL_ALL_FIELDS, bool bAxes = true);
	void DumpFields(CItemPool* Fields, const char* szFilename = NULL,
		int nSel = SEL_ALL_FIELDS, bool bAxes = true);

	void DumpAllFields(const char* szFilename = NULL, 
		int nSel = SEL_ALL_FIELDS, bool bAxes = true);

	void GetSize(int* pSizeX, int* pSizeY) 
		{*pSizeX = m_nSizeX; *pSizeY = m_nSizeY;}

	bool GetField(int nX, int nY, FIELDVAL* pField);
	bool GetField(COORD Coord, FIELDVAL* pField)
		{return GetField(Coord.nX, Coord.nY, pField);}

	int InSelection(FIELDVAL Value, int fSel);
	COORD* GetAroundFields(int *p_nNumFields, COORD Coord, 
		int fSel = SEL_ALL_FIELDS, int nDistance = INDEX_DISTANCE_1);
	COORD* GetAroundFields(int *p_nNumFields, int nX, int nY, 
		int fSel = SEL_ALL_FIELDS, int nDistance = INDEX_DISTANCE_1);

	virtual bool RevealField(int nX, int nY, FIELDVAL RevValue) override;
	virtual bool RevealField(COORD Coord, FIELDVAL RevValue) override
		{return RevealField(Coord.nX, Coord.nY, RevValue);}

	virtual int SolveOneField(COORD Coord) override;
	virtual int SolveOneField(int nX, int nY) override; 

	bool PutMine(COORD MineCoord);
	bool PickUpMine(COORD MineCoord);

	bool CheckForMine(COORD Coord);
	bool CheckForClear(COORD Coord);

	bool Solve(CItemPool* MinedFields, CItemPool* ClearFields);

	bool TestCheckFieldsForMines(COORD Coord, CItemPool* AffectedFields);

	FIELDVAL* FindDevilSolution(COORD Coord);
	////////////////////////////////////////////////////////////////////
protected:
	bool m_bDevilSolution; // je hled�no devil �e�en�
	FIELDVAL* m_pDevilSolution;

	int m_nSizeX, m_nSizeY;
	FIELDVAL* m_pFields; // indexov�no po ��dc�ch

	bool SetField(int nX, int nY, FIELDVAL FieldVal);
	bool SetField(COORD Coord, FIELDVAL FieldVal)
		{return SetField(Coord.nX, Coord.nY, FieldVal);}

	void GetAffectedFields(CItemPool* AffectedFields, COORD Coord);

	void FindApparentFields(CItemPool* MinedFields, CItemPool* ClearFields,
		CItemPool* AffectedFields);

	bool CheckOneField(COORD Coord);
	bool CheckOneField(COORD Coord, COORD* pInvFields, int nInvFields);

	bool CheckFieldsForMines(CItemPool* MinedFields, CItemPool* AffectedFields,
		bool* bGoBack, unsigned long* pBackDeep);

	bool FindBadRecursionDeep(CItemPool* MinedFields, COORD AffField, unsigned long* pBackDeep);
};
