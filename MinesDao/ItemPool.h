#pragma once
#include <stdlib.h>

#define CMEMMAN_BUFF 512

const int E_ITEMPOOL_OK = 0;

////////////////////////////////////////////////////////////////////
class CMemMan abstract 
{
public:
	CMemMan(unsigned long nItemSize, unsigned long nBuff = CMEMMAN_BUFF); 
	~CMemMan() {if(m_pItems) free(m_pItems);}

	unsigned long GetCount() {return m_nItems;}
	unsigned char* GetItem(unsigned long nIndex);

	bool AddItem(unsigned char* pItem);

	void RemoveLast() {if(m_nItems != 0) m_nItems--;}
	void Clear() {m_nItems = 0;}

	bool ToFile(char* szFilename);
	bool FromFile(char* szFilename);

protected:
	unsigned long m_nItemSize;
	unsigned long m_nBuffSize;
	unsigned long m_nItems;
	unsigned char* m_pItems;

	int m_nMemError;
};

////////////////////////////////////////////////////////////////////
class CItemPool : public CMemMan
{
public:
	CItemPool(unsigned long nItemSize,
		int (__cdecl *compare )(const void *, const void *) = NULL,
		unsigned long nBuff = CMEMMAN_BUFF); 

	int GetLastError() {return m_nMemError;}

	unsigned long GetCount() {return m_nItems;}
	unsigned char* GetItem(unsigned long nIndex);
	bool FindItem(unsigned char* pItem, unsigned long* p_nIndex);

	void Sort()
		{if(m_compare) qsort(m_pItems, m_nItems, m_nItemSize, m_compare); }

	bool SetItem(unsigned char* pItem, unsigned long nIndex);
	void RemoveLast() {if(m_nItems != 0) m_nItems--;}
	void Clear() {m_nItems = 0;}

	bool ToFile(char* szFilename);
	bool FromFile(char* szFilename);

////////////////////////////////////////////////////////////////////
protected:
	int (__cdecl *m_compare )(const void *, const void *);

};
