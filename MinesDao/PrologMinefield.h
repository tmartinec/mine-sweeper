#pragma once

#include "MinesDao.h"

using namespace System;
using namespace System::Diagnostics;

class CPrologMinefield : public CMinefield
{
public:
	virtual void Init(int nSizeX, int nSizeY, FIELDVAL FieldsVal, String^ prologPath);

	virtual int SolveOneField(int nX, int nY) override; 
	virtual int SolveOneField(COORD Coord) override;

	virtual bool RevealField(int nX, int nY, FIELDVAL RevValue) override;
	virtual bool RevealField(COORD Coord, FIELDVAL RevValue) override;

	void Close();
private:

	void PrintPrologMinefield(int lines);

	void StartProlog(String^ prologExec, String^ scriptFilename);
	void static RecievePrologLine(Object^ sender, System::Diagnostics::DataReceivedEventArgs^ e);
	void static RecievePrologErrorLine(Object^ sender, System::Diagnostics::DataReceivedEventArgs^ e);
	bool QueryPredicate(String^ predicate) { return QueryPredicate(predicate, true); }
	bool QueryPredicate(String^ predicate, bool waitForProlog);

	void ReadAnswer();
	void WaitForPrologReady();
};
